# Multitrack analysis of circulating tumor DNA
![Multitrack ctDNA analysis](./figs/naive_bayes_model_v7.svg "Multitrack ctDNA")

A new method to identify which lung cancer patients are likely
to benefit (durably!) from immunotherapy using longitudinal measurements of tumor DNA circulating in blood.

## Try it
An online prediction service, based on this code, is available at [liquid-biopsy.ai](https://liquid-biopsy.ai/).

## Statistical analyses
The statistical analyses in the paper are based on the notebooks in the directory
[analyses](analyses).

### Model
The naive Bayes model `NBPomegranate` implements the sci-kit learn API, where
features can be assigned individual distributions. The model architectures as
described in the paper can be found in `ctdna_architecture` and
`clinical_ctdna_architecture`. Specifically, the multitrack features are
modelled using the `InflatedPinnedLogNormal` class, implementing the inflated
log-normal distribution as described in the Supplementary Material.

## Datasets
We do not have permission to freely distribute the datasets that were part of
the analysis. Access to the data will be shared upon reasonable request with
permission of the respective authors of the datasets.


### Requirements
The following Python packages are required to run the analyses (see requirements.txt):
- pomegranate 0.14.8+
- shap
- optuna
- scikit-learn 1+
- openpyxl
- pandas
- vcfpy
- coverage
- pysam
- dill
- seaborn
- biopython

You can use the `Dockerfile` to build an image with all dependencies.

## License
The code is licensed under the open source MIT license. See the [LICENSE.txt](LICENSE.txt) document.