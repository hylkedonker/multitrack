from numpy import argmax, array, ndarray
from pandas import DataFrame, Series
from sklearn.metrics import roc_auc_score

from ctdna.model.classifiers import NBPomegranate


def compute_sample_weights(X: DataFrame) -> ndarray:
    """Sample weights inversely to dataset size."""
    dataset = X.index.map(lambda x: x[:3])
    n_dataset = dataset.value_counts()
    n_slices = len(n_dataset)
    n_total = X.shape[0]
    weights = dataset.map(lambda x: (n_total / n_slices) / n_dataset[x]).to_numpy()
    return weights


def _train_and_predict(distributions: dict, X_train, y_train, X_dev, y_dev) -> float:
    """Train model using distributions and compute roc auc."""
    # Train model.
    weights = compute_sample_weights(X_train)
    features = list(distributions.keys())
    m = NBPomegranate(distributions, pseudo_count=1)
    m.fit(X_train[features], y_train, weights)

    # Compute ROC AUC score on dev set.
    y_dev_prob = m.predict_proba(X_dev[features]).iloc[:, 1]
    return roc_auc_score(y_dev, y_dev_prob)


def relabel_overall_survival(data_frame, inplace=True, landmark: float = 0.0):
    """Generate durable survivor (os >= 1 year) labels.

    Args:
        landmark: Only use patients still alive after `landmark` number of months, and
            compute estimates relative to this point.
    """
    df = data_frame.copy()

    # Filter out events prior to landmark.
    df = df[df["os_months"] >= landmark]

    os_gte_1yr = df["os_months"] >= (12 + landmark)
    os_lt_1yr = (df["os_months"] < (12 + landmark)) & (df["os_event"] == 1)
    os_unknown = ~(os_gte_1yr | os_lt_1yr)

    y_os = Series(-1, index=df.index, dtype=int)
    y_os.loc[os_gte_1yr] = 1
    y_os.loc[os_lt_1yr] = 0
    y_os.loc[os_unknown] = -1

    if inplace:
        df.loc[y_os.index, "class"] = y_os.values
        return df
    return y_os


def roc_auc_objective(
    trial, distributions: dict, X_train, y_train, X_dev, y_dev
) -> float:
    """Optuna objective to tune the pseudo counts on the validation set."""
    unique_distributions = set(distributions.values())
    # Only one type of distribution, use global pseudo_count.
    if len(unique_distributions) == 1:
        pseudo_count = trial.suggest_float("pseudo_count", 1e-2, 1e1, log=True)
    # Tune pseudo_count for each distribution.
    else:
        pseudo_count = {}
        for dist in unique_distributions:
            pseudo_dist = trial.suggest_float(
                f"pseudo_count__{dist.name}", 1e-2, 1e1, log=True
            )
            pseudo_count[dist] = pseudo_dist

    features = list(distributions.keys())

    # Train model.
    weights = compute_sample_weights(X_train)
    m = NBPomegranate(distributions, pseudo_count=pseudo_count)
    m.fit(X_train[features], y_train, weights)

    # Compute ROC AUC score on dev set.
    y_dev_prob = m.predict_proba(X_dev[features]).iloc[:, 1]
    return roc_auc_score(y_dev, y_dev_prob)


def backward_selection(
    distributions: dict, X_train, y_train, X_dev, y_dev, verbose=True
) -> list:
    """Use backward feature selection."""
    feature_bag = sorted(distributions)
    previous_best = _train_and_predict(distributions, X_train, y_train, X_dev, y_dev)
    for _ in range(len(feature_bag)):
        iteration_scores = {}
        for feature in feature_bag:
            proposal_bag = feature_bag.copy()
            proposal_bag.remove(feature)

            distributions_bag = {
                feature: distributions[feature] for feature in proposal_bag
            }
            score = _train_and_predict(
                distributions_bag, X_train, y_train, X_dev, y_dev
            )
            iteration_scores[feature] = score

        scores = array(
            tuple(zip(iteration_scores.keys(), iteration_scores.values())), dtype=object
        )
        best_idx = argmax(scores[:, 1])
        best_feature, best_score = scores[best_idx]
        if verbose:
            print("Best score with", best_feature, "removed", best_score)
            print(f"Improvement {best_score:.2f} - {previous_best:.2f}")
        if best_score - previous_best < 0.0:
            if verbose:
                print("No improvement. Final result:")
                print(feature_bag)
            break
        previous_best = max(best_score, previous_best)
        feature_bag.remove(best_feature)
    return feature_bag
