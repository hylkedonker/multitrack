"""
Naive Bayes model architectures.

There are two model types, both are naive Bayes but different features.
- Only ctDNA (inflated log-Gaussian). Models the distribution of selected genes.
- The same, but with patient characteristics (Bernoulli + inflated log-Gaussian).

This file contains a few helper functions to construct, train, and tune these models.
"""
import optuna
from optuna.samplers import TPESampler

from ctdna.model.classifiers import NBPomegranate
from ctdna.model.distributions import Bernoulli, InflatedPinnedLogNormal
from ctdna.model.utils import compute_sample_weights, roc_auc_objective

GENE_VOCABULARY = {
    "APC",
    "BRCA2",
    "EGFR",
    "NFE2L2",
    "PDGFRA",
    "PIK3CA",
    "TP53",
}


def ctdna_architecture() -> dict:
    """Architecture of model using ctDNA only."""
    return {gene: InflatedPinnedLogNormal for gene in sorted(GENE_VOCABULARY)}


def clinical_ctdna_architecture() -> dict:
    """Architecture of model with clinical characteristics and ctDNA."""
    architecture = ctdna_architecture()
    architecture.update(
        {
            "smoker": Bernoulli,
            "therapyline>1": Bernoulli,
            "histology_squamous": Bernoulli,
        }
    )
    return architecture


def tune_pseudocount(
    distributions: dict, X_train, y_train, X_dev, y_dev, n_trials: int = 100
) -> dict:
    """Tune the pseudo counts and return best parameters"""

    def objective(x):
        return roc_auc_objective(x, distributions, X_train, y_train, X_dev, y_dev)

    sampler = TPESampler(seed=10)  # Make the sampler behave in a deterministic way.
    study = optuna.create_study(direction="maximize", sampler=sampler)
    study.optimize(objective, n_trials=n_trials)

    unique_distributions = set(distributions.values())
    if len(unique_distributions) == 1:
        return study.best_params

    name_to_distr = {dist.name: dist for dist in unique_distributions}
    # Parse distribution names into dict.
    counts = {}
    for name, value in study.best_params.items():
        assert "pseudo_count" in name
        dist_name = name.split("__")[1]
        counts[name_to_distr[dist_name]] = value
    return {"pseudo_count": counts}


def tune_train_model(
    architecture: dict, X_train, y_train, X_dev, y_dev, verbose: bool = True
) -> NBPomegranate:
    """Optimize pseudo-count and retrain model."""

    # Hyperparameter tuning.
    if verbose:
        print("Tuning pseudo counts...")

    hyperparams = tune_pseudocount(architecture, X_train, y_train, X_dev, y_dev)

    if verbose:
        print(hyperparams)
        print("Re-training tuned model... ", end="")

    # Retrain model.
    clf = NBPomegranate(architecture, **hyperparams)
    weights = compute_sample_weights(X_train)
    features = list(architecture.keys())
    clf.fit(X_train[features], y_train, weights)

    if verbose:
        print("[DONE]")
    return clf


def build_train_ctdna_model(
    X_train, y_train, X_dev, y_dev, verbose: bool = True
) -> NBPomegranate:
    """Build and train naive Bayes classifier using only ctDNA information."""
    architecture = ctdna_architecture()
    return tune_train_model(architecture, X_train, y_train, X_dev, y_dev, verbose)


def build_train_clinical_ctdna_model(
    X_train, y_train, X_dev, y_dev, verbose: bool = True
) -> NBPomegranate:
    """Build and train naive Bayes classifier using ctDNA and clinical info."""
    architecture = clinical_ctdna_architecture()
    return tune_train_model(architecture, X_train, y_train, X_dev, y_dev, verbose)
