import re

import vcfpy


def get_header_key(reader: vcfpy.Reader, key: str, default=None):
    """Extract key-value pair from VCF header."""
    matches = [line.value for line in reader.header.lines if line.key == key]
    if matches:
        if len(matches) == 1:
            return matches[0]
        return matches
    return default


def get_snpeff_annotation(record: vcfpy.Record, reader: vcfpy.Reader) -> list[dict]:
    """Turn SnpEff `ANN` INFO element into dictionary.

    Returns:
        Annotation dictionary per allele."""
    annotation = reader.header.get_info_field_info("ANN")
    if annotation.id is None:
        raise KeyError("Unknown INFO field ANN.")

    ann_format = annotation.description
    mo = re.search("'(.+)'", ann_format)
    ann_keys = [key.strip() for key in mo.group(1).split("|")]

    # Harmonise names between VEP and SnpEff.
    field_names = {
        "Gene_Name": "gene",
        "Annotation": "variant_type",
        "HGVS.c": "coding_change",
        "HGVS.p": "amino_acid_change",
    }
    keys = [field_names[key] if key in field_names else key for key in ann_keys]

    return [dict(zip(keys, ann_values.split("|"))) for ann_values in record.INFO["ANN"]]


def get_vep_annotation(record: vcfpy.Record, reader: vcfpy.Reader) -> list[dict]:
    """Turn Ensemble's VEP `CSQ` INFO element into dictionary.

    Returns:
        Annotation dictionary per allele."""
    consequence = reader.header.get_info_field_info("CSQ")
    if consequence.id is None:
        raise KeyError("Unknown INFO field CSQ.")

    csq_format = consequence.description
    csq_keys = csq_format.split("Format: ")[1].split("|")

    # Harmonise names between VEP and SnpEff.
    field_names = {
        "SYMBOL": "gene",
        "Consequence": "variant_type",
        "CodingChange": "coding_change",
        "AminoAcidChange": "amino_acid_change",
    }
    keys = [field_names[key] if key in field_names else key for key in csq_keys]
    result = []
    for consequene_allele in record.INFO["CSQ"]:
        values = consequene_allele.split("|")
        consequence = dict(zip(keys, values))
        if not consequence.get("coding_change", ""):
            # Example: ENST00000419219.1:c.251A>G
            consequence["coding_change"] = consequence.pop("HGVSc", "")
        if not consequence.get("amino_acid_change", ""):
            # Example: ENSP00000404426.1:p.Asn84Ser
            consequence["amino_acid_change"] = consequence.pop("HGVSp", "")
        result.append(consequence)

    return result
