class ParseVCFError(Exception):
    """An error occured while parsing the variant call format (VCF) file."""
