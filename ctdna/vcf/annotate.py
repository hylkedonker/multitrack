"""
Functions to annotate the patogenicity of ClinVar variants.
"""
from pathlib import Path
from typing import Literal, Optional

from Bio import Entrez
import vcfpy

from ctdna.vcf.utils import get_header_key, get_vep_annotation


Entrez.email = "h.c.donker@umcg.nl"


def get_protein_change(entrez_id: str) -> str:
    """Query protein change field of ClinVar document."""
    with Entrez.esummary(db="clinvar", id=entrez_id) as handle:
        record = Entrez.read(handle)

    clinvar_doc = record["DocumentSummarySet"]["DocumentSummary"]
    assert len(clinvar_doc)
    return clinvar_doc[0]["protein_change"]


def is_pathogenic_protein_change(
    gene: str,
    amino_acid_change: str,
    allow_drug_response: bool = False,
) -> bool:
    """Query clinvar to see if the variant is pathogenic."""
    aac_lower = amino_acid_change.lower()
    if "promoter" in aac_lower or "var" in aac_lower or "indel" in aac_lower:
        raise KeyError("Ill posed amino acid change.")

    constraints = (
        '"clinsig likely pathogenic"[Properties] OR "clinsig pathogenic"[Properties] '
    )
    if allow_drug_response:
        constraints += 'OR "clinsig drug response"[Properties]'

    query = f'{gene}[gene] AND "{amino_acid_change}" AND ({constraints})'

    with Entrez.esearch(db="clinvar", retmax=10, term=query) as handle:
        query_result = Entrez.read(handle)
    n_hits = int(query_result["Count"])
    if n_hits == 0:
        return False

    # Since "*" is intepreted as a wildcard instead of termination, verify protein change field.
    for entrez_id in query_result["IdList"]:
        if amino_acid_change.lower() in get_protein_change(entrez_id).lower():
            return True
    return False


def is_pathogenic_variant(
    chrom: str,
    start_pos: int,
    end_pos: Optional[int] = None,
    assembly: Literal["hg19", "hg38"] = "hg19",
    allow_drug_response: bool = False,
) -> bool:
    """Query clinvar using Entrez to see if the variant is pathogenic."""
    chrom_number = chrom.removeprefix("chr")
    assembly_number = 37
    if assembly == "hg38":
        assembly_number = 38

    constraints = (
        '"clinsig likely pathogenic"[Properties] OR "clinsig pathogenic"[Properties] '
    )
    if allow_drug_response:
        constraints += 'OR "clinsig drug response"[Properties]'

    query = f"{chrom_number}[chr] AND {start_pos}"
    if end_pos is not None:
        query += f":{end_pos}"
    query += f"[chrpos{assembly_number}] AND ({constraints})"

    with Entrez.esearch(db="clinvar", retmax=10, term=query) as handle:
        query_result = Entrez.read(handle)

    n_hits = int(query_result["Count"])
    return n_hits > 0


def apply_pathogenic_variant_filter(vcf_in: Path, vcf_out: Path):
    """Filter all variants that are not marked as (likely) pathogenic in clinvar."""
    # Open input, add FILTER header, and open output file
    with vcfpy.Reader.from_path(vcf_in) as reader:
        non_path_filter = [
            ("ID", "NON_PATHOG"),
            (
                "Description",
                "Filter out variants that are not (likely) pathogenic according to clinvar.",
            ),
        ]
        ill_posed_filter = [
            ("ID", "ILL_POSED"),
            (
                "Description",
                "Filter out variants that can not be annotated due to ill posed position.",
            ),
        ]
        reader.header.add_filter_line(vcfpy.OrderedDict(non_path_filter))
        reader.header.add_filter_line(vcfpy.OrderedDict(ill_posed_filter))

        reference_genome = get_header_key(reader, key="reference")
        assert reference_genome in ("hg19", "hg38")

        # Select variants that are (likely) pathogenic according to clinvar => filter the rest.
        with vcfpy.Writer.from_path(vcf_out, reader.header) as writer:
            for record in reader:
                pos = record.POS
                if pos >= 0:
                    if not is_pathogenic_variant(
                        record.CHROM, pos, assembly=reference_genome
                    ):
                        record.add_filter("NON_PATHOG")
                # Thompson et al. dataset without position information. Use protein change.
                else:
                    # For Thompson, there is always just one element.
                    csq = get_vep_annotation(record, reader)[0]
                    gene = csq["gene"]
                    amino_acid_change = csq["amino_acid_change"]
                    try:
                        to_keep = is_pathogenic_protein_change(gene, amino_acid_change)
                    except KeyError:
                        record.add_filter("ILL_POSED")
                    else:
                        if not to_keep:
                            record.add_filter("NON_PATHOG")
                writer.write_record(record)
