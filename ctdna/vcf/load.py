from __future__ import annotations
from pathlib import Path

import pandas as pd
from pandas import DataFrame, MultiIndex, NA, Series, concat
import vcfpy

from ctdna.vcf.errors import ParseVCFError
from ctdna.vcf.utils import get_snpeff_annotation, get_vep_annotation, get_header_key


def get_annotation(record: vcfpy.Record, reader: vcfpy.Reader) -> list[dict]:
    """Attempt to parse variant effect consequences from VCF record.

    Returns:
        Annotation dictionary per allele."""
    try:
        # First try to extract Ensemble's Variant Effect Predictor fields.
        annotations = get_vep_annotation(record, reader)
    except KeyError:
        try:
            # Next try to extract SnpEff fields.
            annotations = get_snpeff_annotation(record, reader)
        except KeyError:
            raise ParseVCFError("Unable to parse variant effect annotations.")

    return annotations


def load_vcf(vcf_path: Path, only_pass: bool = True) -> Series:
    """Load allele frequencies from variant call format file as pandas Series.

    Args:
        only_pass: Filter variants that didn't pass quality criteria.

    Returns:
        Allele frequencies indexed by genomic coordinates and, if availabl, annotation
        information (such as gene, type of variant, HGVS coding and amino acid change).
    """
    with vcfpy.Reader.from_path(vcf_path) as reader:
        patient_id = get_header_key(reader, "patient_id", default=vcf_path.parent.name)

        # Get time point from header, or fallback on filename.
        if (sample_id := get_header_key(reader, "sample_id")) is not None:
            if patient_id is not None and patient_id in sample_id:
                time_point = "t" + sample_id.removeprefix(f"{patient_id}_")
        else:
            time_point = vcf_path.name.lower().removesuffix(".vcf")

        time_weeks = get_header_key(reader, "time (weeks)", NA)
        if isinstance(time_weeks, str):
            if time_weeks:
                time_weeks = float(time_weeks)
            else:
                time_weeks = NA

        annotation_fields = [
            "gene",
            "variant_type",
            "coding_change",
            "amino_acid_change",
        ]

        # Extract allele frequencies, one row per alternate allele of each variant.
        index_header = (time_point, time_weeks)
        variant_index = []
        variant_allele_frequency = []

        for record in reader:
            # Loop through alternate alleles.
            try:
                num_alternates = len(record.INFO["AF"])
            except KeyError:
                # Skip record if there is no allele frequency field.
                continue

            annotations = get_annotation(record, reader)
            for i in range(num_alternates):
                # Filter variants that didn't pass quality criteria.
                if only_pass:
                    if i < len(record.FILTER) and record.FILTER[i] != "PASS":
                        continue

                # Build index as tuple.
                idx = index_header + (record.CHROM, record.POS)
                # Extract allele specific consequence annotations for index.
                idx += tuple(annotations[i][key] for key in annotation_fields)
                variant_index.append(idx)

                allele_freq = float(record.INFO["AF"][i])
                variant_allele_frequency.append(allele_freq)

    # Build multi-index using the following levels.
    index_names = [
        "time_point",
        "time_weeks",
        "chromosome",
        "position",
    ] + annotation_fields
    # Non-shedder row: when no variants were called, make empty data frame with
    # only platform and Patient ID index filled.
    if not variant_index:
        idx = index_header + tuple("" for _ in index_names[len(index_header) :])
        variant_index = [idx]
        variant_allele_frequency = [NA]

    index = pd.MultiIndex.from_tuples(variant_index, names=index_names)
    return Series(variant_allele_frequency, index=index)


def load_vcf_dir(vcf_dir: Path) -> DataFrame:
    """Load variant calls across time points from directory.

    Args:
        vcf_dir: Look for t0.vcf, t1.vcf, .. files in this directory.
    """
    time_points = []
    for vcf_path in sorted(vcf_dir.glob("t*.vcf")):
        try:
            variants = load_vcf(vcf_path)
        except ValueError:
            print(f"Error loading {vcf_path}.")
            raise

        # Average duplicated variant calls (which should technically not exist).
        variants_deduped = variants.groupby(variants.index).mean()
        variants_deduped.index = MultiIndex.from_tuples(
            variants_deduped.index, names=variants.index.names
        )

        # Pivot Series as a multi-index column dataframe (column is time point).
        variants_column = variants_deduped.unstack(level=["time_point", "time_weeks"])
        time_points.append(variants_column)

    variant_frame = concat(time_points, axis="columns")
    # If we have _ANY_ mutation, then remove the non-shedder row.
    is_non_shedder = " and ".join(f'{c} == ""' for c in variant_frame.index.names[2:])
    non_shedder_row = variant_frame.query(is_non_shedder)
    if not non_shedder_row.empty and variant_frame.shape[0] > 1:
        variant_frame = variant_frame.drop(index=non_shedder_row.index)

    # Variants with NaN were not called at a given time point. Technically, they
    # are below the detection limit. But we will simply replace it by 0.
    variant_frame.fillna(0.0, inplace=True)
    return variant_frame
