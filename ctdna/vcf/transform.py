"""
This module contains functions to convert spreadsheets of mutations to VCF files.

These functions are mostly to harmonise the format and domain of input.
"""
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path
import re
from typing import Optional

from numpy import unique
import pandas as pd
from pandas import isna, NA
from pandas.core.frame import DataFrame
import vcfpy
from vcfpy import (
    Header,
    HeaderLine,
    Record,
    SamplesInfos,
    Substitution,
    DEL,
    INDEL,
    INS,
    SNV,
)

BASE_COMPLEMENT = {"A": "T", "C": "G", "T": "A", "G": "C", ".": "."}

CSQ_FIELDS = [
    "Allele",
    "Consequence",
    "CodingChange",
    "AminoAcidChange",
    "SYMBOL",
    "CDS_position",
    "Protein_position",
    "Feature_type",
    "Feature",
    "HGVSc",
]


@dataclass
class _BaseSampleMetaData:
    """Meta data information for writing VCF file."""

    dataset: str
    reference_genome: str
    time_point: int
    platform: str
    patient_id: str
    sample_id: str = ""
    time_weeks: Optional[float] = None


@dataclass
class SampleMetaData(_BaseSampleMetaData):
    def __init__(self, **kwargs):
        """Infer `sample_id` if not given."""
        super().__init__(**kwargs)
        if not self.sample_id:
            self.sample_id = f"{self.patient_id}_{self.time_point}"
        if self.time_weeks is None:
            if self.time_point == 0:
                self.time_weeks = 0.0


def build_header(sample_metadata) -> Header:
    """Construct VCF header with meta data."""
    header = Header(samples=SamplesInfos(sample_names=[]))
    header.add_line(
        HeaderLine(key="fileformat", value="VCFv4.3"),
    )
    header.add_line(
        HeaderLine(key="fileDate", value=datetime.now().strftime(r"%Y%m%d")),
    )
    header.add_line(
        HeaderLine(key="source", value="DonkerPipelinev0.0"),
    )
    header.add_line(HeaderLine(key="reference", value=sample_metadata.reference_genome))
    header.add_line(
        HeaderLine(key="origin", value=sample_metadata.dataset),
    )

    header.add_line(HeaderLine(key="platform", value=sample_metadata.platform))
    header.add_line(HeaderLine(key="patient_id", value=sample_metadata.patient_id))
    header.add_line(HeaderLine(key="sample_id", value=sample_metadata.sample_id))

    time = ""
    if sample_metadata.time_weeks is not None:
        time = str(sample_metadata.time_weeks)
    header.add_line(
        HeaderLine(key="time (weeks)", value=time),
    )

    # Write info lines.
    header.add_info_line(
        {
            "ID": "AF",
            "Number": "A",  # One per alternate allele.
            "Type": "Float",
            "Description": "Allele Frequency",
        }
    )
    header.add_info_line(
        {
            "ID": "MM",
            "Number": "A",  # One per alternate allele.
            "Type": "Float",
            "Description": "Number of mutant molecules per mL",
        }
    )

    header.add_info_line(
        {
            "ID": "DP",
            "Number": 1,
            "Type": "Integer",
            "Description": "Total Depth",
        }
    )

    header.add_info_line(
        {
            "ID": "DB",
            "Number": 0,
            "Type": "Flag",
            "Description": "dbSNP membership",
        }
    )
    header.add_info_line(
        {
            "ID": "AD",
            "Number": "R",
            "Type": "Integer",
            "Description": "Total read depth for each allele",
        }
    )
    header.add_info_line(
        {
            "ID": "END",
            "Number": "1",
            "Type": "Integer",
            "Description": "Stop position of the interval",
        }
    )
    csq_format = "|".join(CSQ_FIELDS)
    header.add_info_line(
        {
            "ID": "CSQ",
            "Number": ".",
            "Type": "String",
            "Description": "Consequence annotations from Ensembl VEP. Format: {}".format(
                csq_format
            ),
        }
    )
    return header


def is_number(x) -> bool:
    """Can variable be serialised to number?"""
    if isna(x):
        return False
    elif isinstance(x, (int, float)):
        return True
    elif isinstance(x, str):
        try:
            float(x)
            return True
        except ValueError:
            return False
    return False


def parse_coding_change(coding_change: str) -> tuple[str, int, str, str]:
    """Parse Human Genome Variation Society (HGVS) style coding change."""
    # TODO: Rely on hgvs package?
    coding_change = coding_change.lower()
    leading_pattern = r"c\.(\d*(?:\+|-)?\d*)"  # Example: c.1394+1
    if "del" in coding_change:
        # Example: c.2236_2250delGAATTAAGAGAAGCA -> position: 2236; ref:
        # GAATTAAGAGAAGCA; alt: -.
        cds_parse = re.search(
            leading_pattern + r"_(\d+)del([ACTG]+)", coding_change, re.IGNORECASE
        )
        position = int(cds_parse.group(1))
        ref = cds_parse.group(3)
        alt = "-"
        variant_type = DEL
    elif "ins" in coding_change:
        # Example: c.2310_2311insGCATACGTGATG -> position 2310; ref: -; alt:
        # GCATACGTGATG.
        cds_parse = re.search(
            leading_pattern + r"_(\d+)ins([ACTG]+)", coding_change, re.IGNORECASE
        )
        position = int(cds_parse.group(1))
        ref = "-"
        alt = cds_parse.group(3)
        variant_type = INS
    elif "delins" in coding_change or "dup" in coding_change:
        raise ValueError(f"Parsing coding change {coding_change} not implemented.")
    else:
        # Example: c.1394+1A>G -> position: 1394+1; ref: A; alt: G.
        cds_parse = re.search(
            leading_pattern + "([ACTG])+>([ACTG]+)", coding_change, re.IGNORECASE
        )
        position = cds_parse.group(1)
        ref = cds_parse.group(2)
        alt = cds_parse.group(3)
        variant_type = SNV

    return variant_type, position, ref.upper(), alt.upper()


def parse_genomic_position(input_str: str) -> tuple:
    """Extract chromosome, start and end position.

    Example:
        chr7:5987371 -> ('chr7', 5987371, None).
    """
    position_mo = re.search(r"(chr(?:\d|X|Y)+):(\d+|\?)(?:-(\d+))?", input_str)
    chrom = position_mo.group(1)
    start_pos = position_mo.group(2)
    end_pos = position_mo.group(3)
    if start_pos != "?":
        start_pos = int(start_pos)
    else:
        start_pos = None
    if end_pos is not None:
        end_pos = int(end_pos)
    return chrom, start_pos, end_pos


def abbreviate_amino_acid(amino_acids: str) -> str:
    """Shorten amino acid symbols using 1 character abbreviation."""
    symbols = {
        "Ala": "A",
        "Cys": "C",
        "Asp": "D",
        "Glu": "E",
        "Phe": "F",
        "Gly": "G",
        "His": "H",
        "Ile": "I",
        "Lys": "K",
        "Leu": "L",
        "Met": "M",
        "Asn": "N",
        "Pro": "P",
        "Gln": "Q",
        "Arg": "R",
        "Ser": "S",
        "Thr": "T",
        "Val": "V",
        "Trp": "W",
        "Tyr": "Y",
        # Protein coding sequence ends at a stop codon.
        "Ter": "*",
    }

    if isna(amino_acids):
        return amino_acids

    for symbol_long, symbol_short in symbols.items():
        amino_acids = amino_acids.replace(symbol_long, symbol_short)
    return amino_acids


def get_safe(item, key, default=None):
    """Get `key` from `item`, with NA values replaced by `default`."""
    value = item.get(key, default=default)
    if isna(value):
        return default
    return value


def parse_variant(variant: pd.Series) -> Record:
    """Turn variant call with annotation into VCF record."""
    protein_position = ""
    amino_acid_change = variant.get(
        "Amino Acid Change", default=variant.get("AA Change", default=NA)
    )

    # Example: p.Lys465Arg -> p.K465R.
    amino_acid_change = abbreviate_amino_acid(amino_acid_change)

    if not isna(amino_acid_change):
        # Example: p.Lys465Arg -> 465. A "*" indicates stop gained.
        aac_match = re.search(r"\w(\d+)(\w|\*)", amino_acid_change)
        if aac_match:
            protein_position = int(aac_match.group(1))
        # In the Thompson et al. they have some variants with free text in the
        # amino acid change field, such as 'Splice Var'. Replace spaces by
        # underscores for valid VCF format.
        else:
            amino_acid_change = amino_acid_change.strip().lower().replace(" ", "_")
    else:
        amino_acid_change = ""

    ref = variant.get("Ref", default=".")
    alt = variant.get("Alt", default=".")
    # Example: c.1394A>G -> type: SNV, position: 1394; ref: A; alt: G.
    coding_change = variant["Coding Change"]
    if not isna(coding_change):
        variant_type, cds_pos, cds_ref, cds_alt = parse_coding_change(coding_change)
        alt_complement = BASE_COMPLEMENT[alt]
        ref_complement = BASE_COMPLEMENT[ref]
        if alt == ".":
            alt = cds_alt
        # Check consistency of parsed alternate allele.
        elif alt != cds_alt:
            # Check for reverse complement.
            if not (alt_complement == cds_alt and ref_complement == cds_ref):
                raise ValueError(
                    f"Extracted {cds_alt} from {coding_change} while alt={alt}."
                    f"Complement alt={alt_complement} and complement ref={ref_complement}."
                )
        if ref == ".":
            ref = cds_ref
        # Check consistency of reference allele.
        elif ref != cds_ref:
            if not (alt_complement == cds_alt and ref_complement == cds_ref):
                raise ValueError(
                    f"Extracted {cds_ref} from {coding_change} while ref={ref}."
                    f"Complement alt={alt_complement} and complement ref={ref_complement}."
                )

    else:
        # Leave blank, because we don't know ref and alt.
        cds_pos = ""
        mutant_class = variant.get("Mutation Class", default="").upper()
        coding_change = ""
        if mutant_class in (SNV, INS, DEL, INDEL):
            variant_type = mutant_class
        else:
            raise KeyError(
                f"Unknown mutation class {mutant_class}.\nIn variant:\n{variant}"
            )

    # Example: chr7:5987371.
    chrom, start_pos, end_pos = parse_genomic_position(variant["Genomic Position"])

    # This field may be missing or, if present, NA. Coalesce both cases.
    variant_id = get_safe(variant, "dbSNP ID", default=".")

    # Existence flag of dbSNP.
    has_variant = False if variant_id == "." else True
    variant_depth = get_safe(variant, "Variant Depth", default=".")
    total_depth = get_safe(variant, "Unique Depth", default=".")

    # Sequencing depth for ref and alt.
    if is_number(total_depth):
        total_depth = int(total_depth)
        if is_number(variant_depth):
            allele_depths = [
                int(total_depth) - int(variant_depth),
                int(variant_depth),
            ]
    else:
        allele_depths = [".", "."]

    filter_status = ["PASS"]
    if is_number(variant_depth):
        if variant_depth < 1:
            filter_status = ["AD<1"]

    transcript = get_safe(variant, "Transcript", default="")
    HGVSc = ""
    if transcript and coding_change:
        HGVSc = transcript + ":" + coding_change

    # Variant effect predictor-like VCF format. For more info see:
    # https://m.ensembl.org/info/docs/tools/vep/vep_formats.html
    CSQ = {
        "Allele": alt,
        # Functional consequence variant description, with "consequence terms"
        # according to the sequence ontology (SO). See also Ensembl Variant
        # Effect Predictor (VEP).
        "Consequence": variant.get("Variant Description", default="")
        .lower()
        .strip()
        .replace(" & ", "&")
        .replace(" ", "_"),
        "CodingChange": coding_change,
        "AminoAcidChange": amino_acid_change,
        "SYMBOL": variant["Gene"],
        "CDS_position": cds_pos,
        "Protein_position": protein_position,
        "Feature_type": "Transcript",
        "Feature": transcript,
        "HGVSc": HGVSc,
    }

    mutant_molecules = get_safe(variant, "No. Mutant Molecules per mL", default=".")

    info = {
        "DB": has_variant,
        # Allele frequency, for each alternate allele.
        "AF": [variant["Allele Fraction"]],
        # Total sequencing depth.
        "DP": total_depth,
        # Read depth of reference and alternate alleles.
        "AD": allele_depths,
        # Number of mutant molecules per alternate allele.
        "MM": [mutant_molecules],
        # Variant consequences.
        "CSQ": ["|".join(str(CSQ[key]) for key in CSQ_FIELDS)],
    }

    # Stop position of INDEL interval, when available.
    if end_pos is not None:
        info["END"] = end_pos

    record = Record(
        CHROM=chrom,
        POS=start_pos if start_pos is not None else -1,
        ID=[variant_id],
        REF=ref,
        ALT=[Substitution(type_=variant_type, value=alt)],
        QUAL=".",
        FILTER=filter_status,
        INFO=info,
        FORMAT=None,
    )

    return record


def to_vcf(variants: pd.DataFrame, metadata: SampleMetaData, output_dir: Path):
    """
    Write the patient's variants (of given time point) to VCF format.
    """
    patient_id = metadata.patient_id
    time_point = metadata.time_point

    # Filter variants according to time point.
    is_t = variants["Sample ID"].apply(lambda x: x.endswith(f"_{time_point}"))
    variants_t = variants[is_t]

    patient_ids = unique(variants_t.index)
    # Check that these samples are the same patient, same time point.
    assert len(patient_ids) <= 1
    assert len(patient_ids) == 0 or patient_ids[0] == patient_id

    filename = f"t{time_point}.vcf"
    output_path = output_dir / patient_id
    output_path.mkdir(parents=True, exist_ok=True)

    header = build_header(metadata)
    writer = vcfpy.Writer.from_path(
        output_path / filename,
        header,
    )

    for _, variant in variants_t.iterrows():
        # Variant not observed.
        if variant["Allele Fraction"] == 0.0:
            continue
        record = parse_variant(variant)
        writer.write_record(record)
    writer.close()


class CtdnaDataset:
    """Contains a ctDNA dataset."""

    def __init__(self, name: str, platform: str, reference_genome: str):
        self.name = name
        self.platform = platform
        self.reference_genome = reference_genome

    def to_vcf(
        self,
        X,
        y,
        labels=["0", "1"],
        target_dir=Path("/tmp/output"),
    ) -> DataFrame:
        """Group variants by patient, and split according to label and time point.

        Returns:
            Dataframe with directories and corresponding labels."""
        assert set(X.loc[y == 0].index).intersection(X.loc[y == 1].index) == set()
        assert set(X.loc[y == 0].index).union(X.loc[y == 1].index) == set(X.index)

        meta_kwargs = {
            "dataset": self.name,
            "platform": self.platform,
            "reference_genome": self.reference_genome,
        }

        # Return data frame with directories and corresponding labels.
        X_result = DataFrame({"class": y.replace({0: labels[0], 1: labels[1]})})

        # Make a subdirectory for each subject, in corresponding label dir.
        for patient_id, y_i in y.iteritems():
            label_i = labels[y_i]
            variants = X[X.index == patient_id]

            time_points = list(
                variants["Sample ID"].apply(lambda x: int(x.split("_")[1])).unique()
            )

            # All data considered here consists of at least two time points:
            # baseline and follow-up. When either of the two are missing =>
            # ctDNA measured, but no variants called.
            if 0 not in time_points:
                time_points.insert(0, 0)
            if 1 not in time_points:
                time_points.insert(1, 1)

            # Save a VCF for each time point.
            for t_i in time_points:
                at_t = variants["Sample ID"] == f"{patient_id}_{t_i}"
                times = variants.loc[at_t, "Time (weeks)"].unique()

                # Time point must be unique, or missing.
                assert len(times) < 2
                t = None
                if times.size > 0:
                    t = times[0]
                elif t_i == 0:  # Set baseline timing to t=0.
                    t = 0.0

                metadata = SampleMetaData(
                    patient_id=patient_id,
                    time_point=t_i,
                    time_weeks=t,
                    **meta_kwargs,
                )
                to_vcf(variants[at_t], metadata, output_dir=target_dir / label_i)

            X_result.loc[patient_id, "directory"] = (
                target_dir / label_i / str(patient_id)
            )

        return X_result
