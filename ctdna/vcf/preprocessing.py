"""
Functions to (help) extract features from variants.
"""
from pathlib import Path
from typing import Callable, Literal, Optional, Union

from numpy import inf, nan, sign, unique
from numpy.testing import assert_array_equal
from pandas import DataFrame, Series, concat

from ctdna.model.architectures import GENE_VOCABULARY
from ctdna.dataset.const import (
    ASSAY_SIZE,
    AVENIO_EXPANDED_PANEL,
    AVENIO_SURVEILLANCE_PANEL,
    GUARDANT360_74_GENES,
    IDES_CAPP_SEQ,
    TARGET_GENES,
    TARGET_CHROMS,
    TEC_SEQ,
)
from ctdna.vcf.load import load_vcf_dir


def pop_platform(x: DataFrame) -> str:
    """Get and drop ctDNA platform index level."""
    platform = x.index.get_level_values("platform")[0]
    x.index = x.index.droplevel("platform")
    return platform


def pop_times(x: DataFrame):
    """Extract `time_weeks` column level."""
    times = x.columns.get_level_values("time_weeks")
    x.columns = x.columns.droplevel(level="time_weeks")
    return times


def relative_change(x: DataFrame, columns=["t0", "t1"]) -> Series:
    """Relative change between reference time point `t_ref` and origin `T_O`."""
    t_O = columns[0]
    t_ref = columns[1]
    dvaf = (x[t_ref] - x[t_O]) / x[t_O]
    # `ratio` is ill posed when denominator is zero.
    # 1) Set to infinite when follow-up is non-zero.
    baseline_clear = (x[t_O] == 0.0) & (x[t_ref] > 0.0)
    dvaf[baseline_clear] = inf
    # 2) Set to zero (no change) when not detected at both time points.
    dvaf[(x[t_O] == 0.0) & (x[t_ref] == 0.0)] = 0
    return dvaf


def select_next_follow_up(x: DataFrame, rename: bool = True) -> DataFrame:
    """Select first follow-up sample 3 weeks < t < 9 weeks."""
    # There are more than two time points.
    if len(x.columns) == 2:
        return x

    for time_point, t_weeks in x.columns[2:]:
        if t_weeks > 3 and t_weeks < 9:
            if not rename:
                return x[["t0", time_point]]
            return x[["t0", time_point]].rename(columns={time_point: "t1"})
    return x[["t0", "t1"]]


def count_ratio(x: DataFrame, filter_synonymous=False) -> DataFrame:
    """Ratio of mutation count between follow-up and baseline."""
    pop_platform(x)
    is_called = x > 0.0

    if filter_synonymous:
        is_synonymous = is_called.reset_index()["variant_type"] == "synonymous_variant"
        n_variants = (
            is_called[~is_synonymous.to_numpy(dtype=bool)].groupby(["Patient ID"]).sum()
        )
    else:
        n_variants = is_called.groupby(["Patient ID"]).sum()

    return DataFrame({"#mutations": relative_change(n_variants) + 1})


def count_mutations(x: DataFrame) -> DataFrame:
    """Platform coverage normalised variant call count."""
    platform = pop_platform(x)
    is_called = x > 0.0
    tmb = is_called.groupby("Patient ID").sum() / ASSAY_SIZE[platform]
    return tmb.rename(columns={"t0": "count(t0)", "t1": "count(t1)"})


def molecular_response_layer(x: DataFrame) -> DataFrame:
    """Relative change of highest baseline variant."""
    pop_platform(x)
    i_max = x["t0"].groupby("Patient ID").idxmax()
    ratio = relative_change(x.loc[i_max]) + 1

    # Verify that there is only one record, and take it.
    assert ratio.groupby("Patient ID").count().sum() == 1
    ratio = ratio.groupby("Patient ID").first()

    result = concat(
        {r"hvaf>50%red": (ratio < 0.5).astype(int), "vaf_ratio": ratio}, axis="columns"
    )
    return result


def molecular_response_layer_old(x: DataFrame) -> DataFrame:
    """Relative change of highest allele fraction between `t0` and `t1`."""
    pop_platform(x)
    is_called = (x > 0.0).groupby("Patient ID").sum()
    clearance = is_called == 0
    non_shedder = clearance["t0"] & clearance["t1"]

    hvaf = x.groupby("Patient ID").max()
    ratio = relative_change(hvaf) + 1

    return concat(
        {
            r"hvaf>50%red": (ratio < 0.5).astype(int),
            "clearance_t1": clearance["t1"].astype(int),
            "clearance_t0+t1": non_shedder.astype(int),
        },
        axis="columns",
    )


def total_vaf(x: DataFrame) -> DataFrame:
    """Compute total (i.e., sum) of VAF."""
    pop_platform(x)
    x_total = x.groupby("Patient ID").sum()
    return x_total.rename(columns={"t0": "vaf(t0)", "t1": "vaf(t1)"})


def mask_unsupported(support: set, vocabulary: set, index) -> DataFrame:
    """Dataframe with zeros where genes not supported by the platform are set to nan.

    Args:
        support: Genes (=columns) in this set are initialised to zero, all others (in
            `vocabulary`) are nan.
        vocabulary: Columns of the dataframe.
        index: Index of dataframe.
    """
    total_vocab = sorted(vocabulary) + ["oov"]
    x = DataFrame(nan, index=[index], columns=total_vocab)
    in_support = support.intersection(total_vocab)
    x[sorted(in_support)] = 0.0
    return x


def filter_not_called(x: DataFrame) -> DataFrame:
    """Drop variants that are not called."""
    # Filter variants where all indices are empty (no gene, coding sequence,
    # variant type, etc).
    not_called = " and ".join(f'{name} == ""' for name in x.index.names[2:])
    x_return = x.drop(index=x.query(not_called).index)
    # Filter where both time points are zero.
    return x_return.loc[~(x_return == 0).all(axis=1)]


def chromosome_layer(x: DataFrame) -> DataFrame:
    """Compute changes per chromosome level."""
    platform = pop_platform(x)
    patient_id = x.index.get_level_values("Patient ID")[0]

    x = filter_not_called(x)

    t1 = x["t1"]
    t0 = x["t0"]
    fraction = 0.5
    relative_change = (abs((t1 - t0) / t0) > fraction) * sign(t1 - t0)

    ups = mask_unsupported(
        support=TARGET_CHROMS[platform],
        vocabulary=TARGET_CHROMS["vocabulary"],
        index=patient_id,
    )
    num_up = (relative_change > 0).groupby(["chromosome"]).sum()
    ups[num_up.index] = (num_up > 0).astype(int)
    ups.rename(columns=lambda x: f"{x}↑", inplace=True)

    downs = mask_unsupported(
        support=TARGET_CHROMS[platform],
        vocabulary=TARGET_CHROMS["vocabulary"],
        index=patient_id,
    )
    num_down = (relative_change < 0).groupby(["chromosome"]).sum()
    downs[num_down.index] = (num_down > 0).astype(int)
    downs.rename(columns=lambda x: f"{x}↓", inplace=True)

    x_chrom = concat([ups, downs], axis="columns")
    return x_chrom[sorted(x_chrom.columns)]


def gene_ratio_layer(x: DataFrame, vocab: Optional[set] = None) -> DataFrame:
    """Compute allele frequency ratio of highest variant. per gene.

    Args:
        vocab: Set of genes to compute ratio, group the remainder as out-of-vocabulary
            (OOV) variants."""
    platform = pop_platform(x)
    patient_id = x.index.get_level_values("Patient ID")[0]
    x = filter_not_called(x).reset_index()

    if vocab is None:
        vocab = GENE_VOCABULARY

    def vocab_encode_gene(idx):
        gene = x.loc[idx, "gene"]
        if gene in vocab:
            return gene
        return "oov"

    i_max = x.groupby(vocab_encode_gene)["t0"].idxmax()
    # Get maximum element (which is the only, and thus first element) with
    # grouped index.
    vaf = x.loc[i_max].groupby(vocab_encode_gene).first()
    dvaf = relative_change(vaf)

    in_vocab = dvaf.index.isin(vocab)
    x_genes = mask_unsupported(
        support=TARGET_GENES[platform].union({"oov"}),
        vocabulary=vocab,
        index=patient_id,
    )
    # This is the crucial part:
    x_genes[dvaf[in_vocab].index] = dvaf[in_vocab]
    if dvaf[~in_vocab].size > 0:
        x_genes[["oov"]] = dvaf[~in_vocab]
    return x_genes + 1


class VcfDataGenerator:
    """Generate matrices from two (or more) longitudinal variant calls in VCF format."""

    def __init__(
        self,
        transformations: Union[list, Callable] = [],
        filters: Union[list, Callable] = [],
        time_points: list = [],
    ):
        """Turn longitudinal variant calls in a row through `transformations`.

        Args:
            transformations: Function that turns a set of variant calls of a single
                instance into features. Output of multiple transformations are
                concatenated.
            filters: Function that filters, per instance, the variants.
            time_points: Which (longitudinal) time points to use.
        """
        self.transformations = transformations
        if callable(transformations):
            self.transformations = [transformations]

        self.filters = filters
        if callable(filters):
            self.filters = [filters]

        self.time_points = time_points
        # Keep track of each `flow_from_{dataframe, directory}` and do a simple
        # column name check.
        self.schema_columns_ = None

    def check_X_y(self, X, y=None):
        """Check internal and schema consistency of features and label."""
        # Check that X and y are congruently ordered.
        if X.index.nlevels > 1:
            X_ptids = X.index.get_level_values("Patient ID")
        else:
            X_ptids = X.index
        X_unique_ptids = [
            X_ptids[i] for i in sorted(unique(X_ptids, return_index=True)[1])
        ]
        if y is not None:
            assert_array_equal(X_unique_ptids, y.index)

        # Make sure no schema changes have occured.
        if self.schema_columns_ is None:
            self.schema_columns_ = X.columns
        else:
            if len(self.schema_columns_) != len(X.columns):
                raise LookupError(
                    "Current schema is incongruent with previous `flow_from_*` "
                    "call. Dimension mismatch."
                )
            elif not all(self.schema_columns_ == X.columns):
                raise LookupError(
                    "Current schema is incongruent with previous `flow_from_*` "
                    "call. Column name mismatch."
                )

    def check_x(self, x_i, patient_id):
        """Check that Patient ID in x_i is unique."""
        if x_i.index.nlevels == 1:
            x_i.index.name = "Patient ID"
            indices = x_i.index
        else:
            indices = x_i.index.get_level_values("Patient ID")

        assert indices.unique().size == 1
        # When the given index does not match `patient_id`, then enforce it.
        if indices[0] != patient_id:
            x_i.rename(index={indices[0]: patient_id}, level="Patient ID", inplace=True)

    def flow_from_dataframe(
        self,
        dataframe: DataFrame,
        directory: Optional[str] = None,
        x_col: str = "directory",
        y_col: Optional[str] = "class",
        class_mode: Optional[Literal["categorical"]] = "categorical",
        keep_columns: bool = True,
        drop_time: bool = True,
    ) -> Union[DataFrame, tuple[DataFrame]]:
        """Load dataset by reading paths and target label from dataframe.

        Args:
            dataframe: Requires multiindex with `Patient ID` and `platform`.
            keep_columns: Passthrough all columns except `x_col` and `y_col` in
                resulting dataframe.
            class_mode: When None, don't extract target label (inference mode).
            x_col: Column pointing to location of VCF files.
            y_col: Target label column.
            class_mode: Return features and labels during training mode ("categorical"),
                or return only features during serving (None).
            keep_columns: Use only the features extracted through `x_col` (False) or
                also concatenate other columns in the dataframe after extraction (True).
        """
        root_path = Path()
        if directory is not None:
            root_path = Path(directory)

        if keep_columns:
            to_keep = dataframe.columns.difference([x_col, y_col])
            if to_keep.size > 0 and not self.transformations:
                raise NotImplementedError(
                    "Identity operator not implemented for `flow_from_dataframe`."
                )

        X = []
        y = []
        # Loop trough all label directories.
        for index, target_dir in dataframe[x_col].iteritems():
            sample_dir = Path(target_dir)
            if not sample_dir.is_absolute():
                sample_dir = root_path / sample_dir

            x_raw = load_vcf_dir(sample_dir)
            # Prepend dataframe indices.
            x_raw = concat([x_raw], keys=[index], names=dataframe.index.names)
            x_i = self.apply_transform(x_raw, drop_time)

            # Concatenate other columns.
            if keep_columns:
                if to_keep.size > 0:
                    # Drop all levels except the first before merging with `x_i`.
                    redundant_levels = list(range(1, dataframe.index.nlevels))
                    x_passthrough = dataframe.loc[[index], to_keep].droplevel(
                        redundant_levels
                    )

                    x_i = concat([x_i, x_passthrough], axis="columns")

            patient_id = index[0]
            self.check_x(x_i, patient_id)

            X.append(x_i)
            if class_mode is not None:
                y.append(
                    {
                        "Patient ID": patient_id,
                        "label": dataframe.loc[index, y_col],
                    }
                )

        X_data_frame = concat(X, axis="rows")
        if class_mode is not None:
            y = DataFrame(y).set_index("Patient ID")
            if y.size > 1:
                y = y.squeeze()

            self.check_X_y(X_data_frame, y)
            return X_data_frame, y
        # No need to check y, return X only.
        self.check_X_y(X_data_frame, None)
        return X_data_frame

    def flow_from_directory(
        self,
        directory: str,
        drop_time: bool = True,
        class_mode: Optional[Literal["categorical"]] = "categorical",
        index_name: str = "Patient ID",
    ) -> Union[DataFrame, tuple[DataFrame]]:
        """Load dataset and infer labels from given directory.

        Args:
            index_name: Assign this name to the index.
        """
        dir_path = Path(directory)
        targets = sorted(c for c in dir_path.iterdir() if c.is_dir())
        y = []
        X = []
        # Loop trough all label directories.
        for target_dir in targets:
            target_label = target_dir.name
            # Loop through all samples.
            for sample_dir in sorted(target_dir.iterdir()):
                if not sample_dir.is_dir():
                    continue

                patient_id = sample_dir.name
                y.append([patient_id, target_label])

                x_raw = load_vcf_dir(sample_dir)
                # Prepend Patient ID index.
                x_raw = concat([x_raw], keys=[patient_id], names=[index_name])
                x_i = self.apply_transform(x_raw, drop_time=drop_time)

                self.check_x(x_i, patient_id)

                X.append(x_i)

        X_data_frame = concat(X, axis="rows")
        if class_mode is not None:
            y = DataFrame(y, columns=[index_name, "y"]).set_index(index_name)
            if y.size > 1:
                y = y.squeeze()
            self.check_X_y(X_data_frame, y)

            return X_data_frame, y
        # No need to check y, return X only.
        self.check_X_y(X_data_frame, None)
        return X_data_frame

    def apply_transform(
        self, x: DataFrame, drop_time: bool = True
    ) -> Union[DataFrame, Series]:
        """Transform single set of variant calls."""
        if self.filters:
            # Iteratively apply filters.
            for variant_filter in self.filters:
                x = variant_filter(x)

        if self.time_points and set(self.time_points).issubset(
            x.columns.get_level_values("time_point")
        ):
            x = x[self.time_points]

        # Drop time_weeks column from multi-index column.
        if drop_time:
            x = x.droplevel(level="time_weeks", axis="columns")

        # Concatenate transformation columns.
        if self.transformations:
            x_transformed = [f(x.copy()) for f in self.transformations]
            if len(x_transformed) == 1:
                return x_transformed[0]
            return concat(x_transformed, axis="columns")

        # No transformations -> identity (do-nothing) operation.
        return x
