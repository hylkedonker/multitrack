from cycler import cycler
from typing import Callable

from matplotlib import pyplot as plt
import matplotlib.colors as mcolors
import numpy as np
from numpy import isinf, isnan
import matplotlib.pyplot as pl
from shap.plots._labels import labels
from shap.utils import safe_isinstance, format_value
from shap.plots import colors

from pandas import DataFrame, Series
import seaborn as sns
from sklearn.metrics import (
    accuracy_score,
    average_precision_score,
    confusion_matrix,
    precision_recall_curve,
    roc_curve,
    roc_auc_score,
)


from ctdna.model.architectures import GENE_VOCABULARY
from ctdna.utils import bootstrap_score, stratify_by_dataset


def plot_stratified_roc_curve(
    y_true, y_pred, stratifier: Callable[[Series], dict] = stratify_by_dataset
):
    """Make ROC curve stratified by dataset."""
    ax = plt.gca()
    ax.set(
        xlim=[-0.05, 1.05],
        ylim=[-0.05, 1.05],
        title="Receiver operating characteristic",
    )

    i = 0
    linestyles = ["--", "-", "-.", ":"]
    for paper, y_pred_i in stratifier(y_pred).items():
        if paper == "overall":
            continue

        y_true_i = y_true[y_pred_i.index]
        fpr, tpr, _ = roc_curve(y_true_i, y_pred_i)
        auc_score = bootstrap_score(y_true_i, y_pred_i, roc_auc_score)
        auc_txt = confidence_interval_label(auc_score)
        n_label = f"$n={y_pred_i.size}$"
        label = f"{paper};\nAUC: {auc_txt} ({n_label})"
        ax.plot(fpr, tpr, linestyles[i % 4], lw=2, label=label)
        i += 1

    ax.set_xlabel("False positive rate")
    ax.set_ylabel("True positive rate")
    ax.legend(frameon=False, bbox_to_anchor=(1.00, 1.05))


def plot_confusion_matrix(y_true, y_pred, ax, labels):
    """Make confusion matrix with operating point at Youden J."""

    confusion = DataFrame(
        confusion_matrix(y_true, y_pred, labels=[0, 1]),
        columns=labels,
        index=labels,
    )
    sns.heatmap(
        confusion,
        annot=confusion,
        fmt="",
        cmap=plt.cm.Blues,
        ax=ax,
        cbar=False,
    )

    accuracy = bootstrap_score(y_true, y_pred, accuracy_score)
    accuracy_txt = confidence_interval_label(accuracy)
    title = f"Acc. {accuracy_txt}"
    ax.tick_params(axis="x", labelrotation=0)

    ax.set_title(title)
    ax.set_ylabel("Actual", weight="bold")
    ax.set_xlabel("Predicted", weight="bold")


def plot_precision_recall(y_true, y_pred):
    """
    Plot precision recall curve, with bootstrapped average precision.
    """
    precision, recall, _ = precision_recall_curve(y_true, y_pred)

    # Compute average precision, with 95 % CI.
    p_score = bootstrap_score(y_true, y_pred, average_precision_score)
    p_label = confidence_interval_label(p_score)
    label = f"Average precision: {p_label}"

    plt.rc("font", family="serif")
    # fig, ax = plt.subplots()
    ax = plt.gca()
    ax.set(
        xlim=[-0.05, 1.05],
        ylim=[-0.05, 1.05],
        # title="Precision-recall curve",
    )
    ax.plot(recall, precision, label=label, lw=2)
    ax.set_xlabel("Recall")
    ax.set_ylabel("Precision")
    ax.legend(frameon=False, loc="best")
    ax.set_xlim([0, 1])


def plot_roc_curve(y_true, y_prob, threshold: float, labels, ax=None):
    if ax is None:
        ax = plt.gca()

    fpr, tpr, _ = roc_curve(y_true, y_prob)
    auc_score = bootstrap_score(y_true, y_prob, roc_auc_score)
    auc_label = confidence_interval_label(auc_score)
    n_label = f"$n={y_true.size}$"
    label = f"AUC: {auc_label} ({n_label})"
    ax.plot(fpr, tpr, lw=2, label=label)

    y_pred = (y_prob > threshold).astype(int)
    # Inset the confusion matrix.
    inset_ax = ax.inset_axes([0.55, 0.25, 0.35, 0.35])
    plot_confusion_matrix(y_true, y_pred, inset_ax, labels=labels)

    # Update title with J value.
    inset_ax.set_title(
        inset_ax.get_title().replace("Acc.", rf"Acc($\Lambda$={threshold:.2f}):")
    )

    ax.set_xlabel("False positive rate")
    ax.set_ylabel("True positive rate")
    ax.legend(frameon=False, loc="upper left")


def confidence_interval_label(score, latex: bool = True) -> str:
    """Turn value with confidence interval into text.

    Args:
        latex: Format string as LaTeX math.
    """
    value, lower, upper = score["point"], score["lower"], score["upper"]
    label_args = (
        value,
        upper - value,
        lower - value,
    )
    if latex:
        return "{:.2f}$^{{+{:.2f}}}_{{{:.2f}}}$".format(*label_args)
    return f"{value:.2f} (95 % CI: {lower:.2f}-{upper:.2f})"


def waterfall(
    expected_value,
    shap_values=None,
    features=None,
    feature_names=None,
    max_display=10,
    target_label=None,
    logit=False,
    show=True,
):
    """Plots an explantion of a single prediction as a waterfall plot.

    The SHAP value of a feature represents the impact of the evidence provided
    by that feature on the model's output. The waterfall plot is designed to
    visually display how the SHAP values (evidence) of each feature move the
    model output from our prior expectation under the background data
    distribution, to the final model prediction given the evidence of all the
    features. Features are sorted by the magnitude of their SHAP values with the
    smallest magnitude features grouped together at the bottom of the plot when
    the number of features in the models exceeds the max_display parameter.

    Parameters
    ----------
    expected_value : float This is the reference value that the feature
        contributions start from. For SHAP values it should be the value of
        explainer.expected_value. shap_values : numpy.array One dimensional
        array of SHAP values. features : numpy.array One dimensional array of
        feature values. This provides the values of all the features, and should
        be the same shape as the shap_values argument. feature_names : list List
        of feature names (# features). max_display : str The maximum number of
        features to plot. show : bool Whether matplotlib.pyplot.show() is called
        before returning. Setting this to False allows the plot to be customized
        further after it has been created.
    """

    # Turn off interactive plot
    if show is False:
        plt.ioff()

    # support passing an explanation object
    upper_bounds = None
    lower_bounds = None

    # make sure we only have a single output to explain
    if (type(expected_value) == np.ndarray and len(expected_value) > 0) or type(
        expected_value
    ) == list:
        raise Exception(
            "waterfall_plot requires a scalar expected_value of the model output as the first "
            "parameter, but you have passed an array as the first parameter! "
            "Try shap.waterfall_plot(explainer.expected_value[0], shap_values[0], X[0]) or "
            "for multi-output models try "
            "shap.waterfall_plot(explainer.expected_value[0], shap_values[0][0], X[0])."
        )

    # make sure we only have a single explanation to plot
    if len(shap_values.shape) == 2:
        raise Exception(
            "The waterfall_plot can currently only plot a single explanation but a matrix of explanations was passed!"
        )

    # unwrap pandas series
    if safe_isinstance(features, "pandas.core.series.Series"):
        if feature_names is None:
            feature_names = list(features.index)
        features = features.values

    # fallback feature names
    if feature_names is None:
        feature_names = np.array(
            [labels["FEATURE"] % str(i) for i in range(len(shap_values))]
        )

    # init variables we use for tracking the plot locations
    num_features = min(max_display, len(shap_values))
    row_height = 0.5
    rng = range(num_features - 1, -1, -1)
    order = np.argsort(-np.abs(shap_values))
    pos_lefts = []
    pos_inds = []
    pos_widths = []
    pos_low = []
    pos_high = []
    neg_lefts = []
    neg_inds = []
    neg_widths = []
    neg_low = []
    neg_high = []
    loc = expected_value + shap_values.sum()
    yticklabels = ["" for i in range(num_features + 1)]

    # size the plot based on how many features we are plotting
    pl.gcf().set_size_inches(8, num_features * row_height + 1.5)

    # see how many individual (vs. grouped at the end) features we are plotting
    if num_features == len(shap_values):
        num_individual = num_features
    else:
        num_individual = num_features - 1

    # compute the locations of the individual features and plot the dashed connecting lines
    for i in range(num_individual):
        sval = shap_values[order[i]]
        loc -= sval
        if sval >= 0:
            pos_inds.append(rng[i])
            pos_widths.append(sval)
            if lower_bounds is not None:
                pos_low.append(lower_bounds[order[i]])
                pos_high.append(upper_bounds[order[i]])
            pos_lefts.append(loc)
        else:
            neg_inds.append(rng[i])
            neg_widths.append(sval)
            if lower_bounds is not None:
                neg_low.append(lower_bounds[order[i]])
                neg_high.append(upper_bounds[order[i]])
            neg_lefts.append(loc)
        if num_individual != num_features or i + 4 < num_individual:
            pl.plot(
                [loc, loc],
                [rng[i] - 1 - 0.4, rng[i] + 0.4],
                color="#bbbbbb",
                linestyle="--",
                linewidth=0.5,
                zorder=-1,
            )
        if features is None:
            yticklabels[rng[i]] = feature_names[order[i]]
        else:
            name_i = feature_names[order[i]]
            value_i = features[order[i]]
            yticklabels[rng[i]] = f"{name_i}: {value_i}"

    # add a last grouped feature to represent the impact of all the features we didn't show
    if num_features < len(shap_values):
        yticklabels[0] = "%d other features" % (len(shap_values) - num_features + 1)
        remaining_impact = expected_value - loc
        if remaining_impact < 0:
            pos_inds.append(0)
            pos_widths.append(-remaining_impact)
            pos_lefts.append(loc + remaining_impact)
            c = colors.red_rgb
        else:
            neg_inds.append(0)
            neg_widths.append(-remaining_impact)
            neg_lefts.append(loc + remaining_impact)
            c = colors.blue_rgb

    points = (
        pos_lefts
        + list(np.array(pos_lefts) + np.array(pos_widths))
        + neg_lefts
        + list(np.array(neg_lefts) + np.array(neg_widths))
    )
    dataw = np.max(points) - np.min(points)

    # draw invisible bars just for sizing the axes
    label_padding = np.array([0.1 * dataw if w < 1 else 0 for w in pos_widths])
    pl.barh(
        pos_inds,
        np.array(pos_widths) + label_padding + 0.02 * dataw,
        left=np.array(pos_lefts) - 0.01 * dataw,
        color=colors.red_rgb,
        alpha=0,
    )
    label_padding = np.array([-0.1 * dataw if -w < 1 else 0 for w in neg_widths])
    pl.barh(
        neg_inds,
        np.array(neg_widths) + label_padding - 0.02 * dataw,
        left=np.array(neg_lefts) + 0.01 * dataw,
        color=colors.blue_rgb,
        alpha=0,
    )

    # define variable we need for plotting the arrows
    head_length = 0.08
    bar_width = 0.8
    xlen = pl.xlim()[1] - pl.xlim()[0]
    fig = pl.gcf()
    ax = pl.gca()
    xticks = ax.get_xticks()
    bbox = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    width, height = bbox.width, bbox.height
    bbox_to_xscale = xlen / width
    hl_scaled = bbox_to_xscale * head_length
    renderer = fig.canvas.get_renderer()

    # draw the positive arrows
    for i in range(len(pos_inds)):
        dist = pos_widths[i]
        arrow_obj = pl.arrow(
            pos_lefts[i],
            pos_inds[i],
            max(dist - hl_scaled, 0.000001),
            0,
            head_length=min(dist, hl_scaled),
            color=colors.red_rgb,
            width=bar_width,
            head_width=bar_width,
        )

        if pos_low is not None and i < len(pos_low):
            pl.errorbar(
                pos_lefts[i] + pos_widths[i],
                pos_inds[i],
                xerr=np.array(
                    [[pos_widths[i] - pos_low[i]], [pos_high[i] - pos_widths[i]]]
                ),
                ecolor=colors.light_red_rgb,
            )

        txt_obj = pl.text(
            pos_lefts[i] + 0.5 * dist,
            pos_inds[i],
            format_value(pos_widths[i], "%+0.02f"),
            horizontalalignment="center",
            verticalalignment="center",
            color="white",
            fontsize=12,
        )
        text_bbox = txt_obj.get_window_extent(renderer=renderer)
        arrow_bbox = arrow_obj.get_window_extent(renderer=renderer)

        # if the text overflows the arrow then draw it after the arrow
        if text_bbox.width > arrow_bbox.width:
            txt_obj.remove()

            txt_obj = pl.text(
                pos_lefts[i] + (5 / 72) * bbox_to_xscale + dist,
                pos_inds[i],
                format_value(pos_widths[i], "%+0.02f"),
                horizontalalignment="left",
                verticalalignment="center",
                color=colors.red_rgb,
                fontsize=12,
            )

    # draw the negative arrows
    for i in range(len(neg_inds)):
        dist = neg_widths[i]

        arrow_obj = pl.arrow(
            neg_lefts[i],
            neg_inds[i],
            -max(-dist - hl_scaled, 0.000001),
            0,
            head_length=min(-dist, hl_scaled),
            color=colors.blue_rgb,
            width=bar_width,
            head_width=bar_width,
        )

        if neg_low is not None and i < len(neg_low):
            pl.errorbar(
                neg_lefts[i] + neg_widths[i],
                neg_inds[i],
                xerr=np.array(
                    [[neg_widths[i] - neg_low[i]], [neg_high[i] - neg_widths[i]]]
                ),
                ecolor=colors.light_blue_rgb,
            )

        txt_obj = pl.text(
            neg_lefts[i] + 0.5 * dist,
            neg_inds[i],
            format_value(neg_widths[i], "%+0.02f"),
            horizontalalignment="center",
            verticalalignment="center",
            color="white",
            fontsize=12,
        )
        text_bbox = txt_obj.get_window_extent(renderer=renderer)
        arrow_bbox = arrow_obj.get_window_extent(renderer=renderer)

        # if the text overflows the arrow then draw it after the arrow
        if text_bbox.width > arrow_bbox.width:
            txt_obj.remove()

            txt_obj = pl.text(
                neg_lefts[i] - (5 / 72) * bbox_to_xscale + dist,
                neg_inds[i],
                format_value(neg_widths[i], "%+0.02f"),
                horizontalalignment="right",
                verticalalignment="center",
                color=colors.blue_rgb,
                fontsize=12,
            )

    pl.yticks(range(num_features), yticklabels[:-1])

    # # put horizontal lines for each feature row
    # for i in range(num_features):
    #     pl.axhline(i, color="#cccccc", lw=0.5, dashes=(1, 5), zorder=-1)

    # mark the prior expected value and the model prediction
    pl.axvline(
        expected_value,
        0,
        1 / num_features,
        color="#bbbbbb",
        linestyle="--",
        linewidth=0.5,
        zorder=-1,
    )
    fx = expected_value + shap_values.sum()
    pl.axvline(fx, 0, 1, color="#bbbbbb", linestyle="--", linewidth=0.5, zorder=-1)

    # clean up the main axis
    pl.gca().xaxis.set_ticks_position("bottom")
    pl.gca().yaxis.set_ticks_position("none")
    pl.gca().spines["right"].set_visible(False)
    pl.gca().spines["top"].set_visible(False)
    pl.gca().spines["left"].set_visible(False)
    ax.tick_params(labelsize=13)

    if target_label is not None:
        if logit:
            plt.xlabel(f"Probability {target_label}", fontsize=14)
        else:
            plt.xlabel(f"Log likelihood ratio {target_label}")

    if logit:
        locs, _ = pl.xticks()
        f_logit = lambda x: 1 / (1 + np.exp(-x))
        pl.xticks(locs, [f"{f_logit(x):.2f}" for x in locs])

    if show:
        pl.show()
    else:
        return pl.gcf()


def polish_features(X_in: DataFrame) -> DataFrame:
    """Polish feature values for waterfall plot."""
    ratios = sorted(GENE_VOCABULARY)
    not_ratio = ~X_in.columns.isin(ratios)
    others = X_in.columns[not_ratio]
    print(others)
    X_out = X_in.join(Series("prior", name="label", index=X_in.index)).copy()

    def to_percentage(x):
        """Format as per centage."""
        if isnan(x):
            return "-"
        elif isinf(x):
            return "+∞%"
        percentage = (x - 1) * 100
        return f"{percentage:+.0f}%"

    def to_int(x):
        if isnan(x):
            return "-"
        elif x.is_integer():
            return str(int(x))
        return x

    X_out[ratios] = X_out[ratios].applymap(to_percentage)
    X_out[others] = X_out[others].applymap(to_int)
    return X_out


def plot_variants(X: DataFrame, ax=None):
    """Show variant allele frequency as a function of time."""
    if ax is None:
        ax = plt.gca()

    linestyle_cycler = cycler("linestyle", ["-", "--", ":", "-."]) * cycler(
        color=list(mcolors.TABLEAU_COLORS)
    )
    X = X.dropna(axis="columns", how="all").copy()
    with plt.rc_context():
        plt.rc("axes", prop_cycle=linestyle_cycler)

        for variant_index, variant_af in X.iterrows():
            gene = variant_index[2]
            #             chrom, pos, gene = variant_index[:3]
            #             coding_change = variant_index[4]
            label = f"{gene}"
            time = variant_af.index.get_level_values("time_weeks")
            ax.plot(time, variant_af, marker="o", label=label)

        ax.set_ylim([0, 1])
        n_variants = X.shape[0]
        ax.legend(
            frameon=False,
            bbox_to_anchor=(1.1, 1.05),
            ncol=(1 if n_variants < 10 else 2),
        )
        ax.set_yscale("symlog", linthresh=0.0001)
        ax.set_xlabel("Time (weeks)")
        ax.set_ylabel("Variant allele frequency (-)")
