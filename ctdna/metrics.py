from sklearn.metrics import roc_curve
from pandas import Series
from sklearn.metrics import (
    accuracy_score,
    average_precision_score,
    f1_score,
    roc_auc_score,
)

from ctdna.statistics import bootstrap_score
from ctdna.plot.views import confidence_interval_label


def youden_j(y_true, y_pred) -> float:
    """Compute threshold correspoding to Youden's J."""
    fpr, tpr, thresholds = roc_curve(y_true, y_pred, pos_label=1)
    j_scores = tpr - fpr
    j_ordered = sorted(zip(j_scores, thresholds))
    return j_ordered[-1][1]


def tpr(y_true, y_prob, J: float = 0.5) -> float:
    """Evaluate true positive rate (TPR) at operating point J."""
    y_pred = y_prob > J
    positives = sum(y_true)
    true_positives = sum(y_true.astype(bool) & y_pred)
    return true_positives / positives


def fpr(y_true, y_prob, J: float = 0.5) -> float:
    """Evaluate false positive rate (FPR) at operating point J."""
    y_pred = y_prob > J
    negatives = y_true.size - sum(y_true)
    # Actual negative, but classified as positive.
    false_positives = sum((~y_true.astype(bool)) & y_pred)
    return false_positives / negatives


def sensitivity(y_true, y_prob, J: float = 0.5) -> float:
    """Sensitivity is an alias for true positive rate."""
    return tpr(y_true, y_prob, J)


def specificity(y_true, y_prob, J: float = 0.5) -> float:
    """Compute specificity at operating point J."""
    return 1 - fpr(y_true, y_prob, J)


def classification_report(y_true, y_prob, threshold: float, latex=True) -> Series:
    """Compute bootstrapped metrics and confidence intervals."""
    specification = Series(
        0,
        index=[
            "ROC AUC",
            "Accuracy",
            "Sensitivity",
            "Specificity",
            "Average precision",
            "$F_1$",
        ],
    )
    y_pred = y_prob > threshold

    roc_auc = bootstrap_score(y_true, y_prob, metric=roc_auc_score)
    specification["ROC AUC"] = confidence_interval_label(roc_auc, latex)

    sens = bootstrap_score(y_true, y_pred, metric=sensitivity)
    specification["Sensitivity"] = confidence_interval_label(sens, latex)

    spec = bootstrap_score(y_true, y_pred, metric=specificity)
    specification["Specificity"] = confidence_interval_label(spec, latex)

    accuracy = bootstrap_score(y_true, y_pred, metric=accuracy_score)
    specification["Accuracy"] = confidence_interval_label(accuracy, latex)

    p_score = bootstrap_score(y_true, y_prob, average_precision_score)
    specification["Average precision"] = confidence_interval_label(p_score, latex)

    f1 = bootstrap_score(y_true, y_pred, f1_score)
    specification["$F_1$"] = confidence_interval_label(f1, latex)

    return specification
