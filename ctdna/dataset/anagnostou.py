"""
Load and transform patient characteristics and mutation data from Anagnostou et al..

Extracts the data from spreadsheets (extracted from the paper's supporting information),
generates variant call format (VCF) files and outputs CSV files with harmonised patient
characteristics.

Anagnostou et al. "Dynamics of tumor and immune responses during immune checkpoint
blockade in non–small cell lung cancer." Cancer research 79.6 (2019): 1214-1225.
"""

from pathlib import Path

from numpy import nan
from pandas import concat, read_excel, DataFrame

from ctdna.dataset.const import TEC_SEQ
from ctdna.vcf.transform import CtdnaDataset


PATIENT_ID_PREFIX = "ANAG"


def relabel_patientid(input_index: str) -> str:
    return PATIENT_ID_PREFIX + input_index.strip("CGLU")


def _load_anagnostou_variants(
    excel_filename,
) -> DataFrame:
    """Load Anagnostou variant calls from spreadsheet and data cleanse."""
    X_variants = read_excel(excel_filename)
    X_variants["Patient ID"] = X_variants["Patient ID"].apply(relabel_patientid)
    X_variants["Sample ID"] = X_variants["Sample ID"].apply(relabel_patientid)
    X_variants["Variant Description"] = X_variants["Variant Description"].replace(
        {
            "Frameshift": "Frameshift variant",
            "Nonsense": "Stop gained",
            "In-frame deletion": "Inframe deletion",
            "Nonsynonymous coding": "Missense variant",
            "Splice site donor": "Splice donor variant",
        }
    )
    return X_variants.set_index("Patient ID")


def _load_anagnostou_annotations(
    excel_filename,
) -> DataFrame:
    X_clinic = read_excel(excel_filename)
    X_clinic["Patient ID"] = X_clinic["Patient ID"].apply(relabel_patientid)
    X_clinic = X_clinic.set_index("Patient ID")
    X_clinic["stage_IV"] = X_clinic.pop("stage").replace({"IV": 1, "III": 0})
    X_clinic["histology_squamous"] = X_clinic.pop("histology").replace(
        {"Squamous Cell Carcinoma": 1, "Adenocarcinoma": 0}
    )
    X_clinic["therapyline>1"] = nan
    X_clinic["therapy"] = X_clinic["therapy"].replace(
        {"nivolumab": "αPD-1", "pembrolizumab": "αPD-1"}
    )
    X_clinic["PS (ECOG) ≥ 2"] = nan
    return X_clinic


def load_anagnostou_to_vcf(target_dir=Path("artifacts/vcf/anagnostou")):
    """Load variants and store as VCF on disk."""
    current_path = Path(__file__).parent.resolve()
    clinic_path = current_path / "raw/Anagnostou/clinical.xlsx"
    variant_path = current_path / "raw/Anagnostou/variants.xlsx"

    X_clinic = _load_anagnostou_annotations(clinic_path)
    X_variants = _load_anagnostou_variants(variant_path)

    pfs_resp = (X_clinic["pfs_months"] >= 6).astype(int)

    # Check that labels are survival wise consistent.
    assert all(X_clinic["pfs_months"] <= X_clinic["os_months"])
    assert all(X_clinic[~pfs_resp.astype(bool)]["pfs_event"] == 1)

    dataset = CtdnaDataset(
        name="Anagnostou et al., Cancer Research (2019)",
        platform=TEC_SEQ,
        reference_genome="hg19",
    )
    pfs_labels = ["pfs<6months", "pfs≥6months"]
    X_pfs = dataset.to_vcf(
        X_variants,
        pfs_resp,
        labels=pfs_labels,
        target_dir=target_dir / "pfs",
    )
    # Generate dataframe with directories and corresponding labels.
    X = concat([X_clinic, X_pfs], axis="columns")
    X["platform"] = TEC_SEQ
    X.to_csv(target_dir / "pfs.csv")

    # N.B., we don't have the information for overall survival > 12 months since
    # the following exception fails:

    # os_resp = (X_clinic['os_months'] >= 12).astype(int)
    # assert all(X_clinic[~os_resp.astype(bool)]["os_event"] == 1)
    # dataset.to_vcf(
    #     X_variants,
    #     os_resp,
    #     labels=["os<1yr", "os≥1yr"],
    #     target_dir=target_dir / "os",
    # )
