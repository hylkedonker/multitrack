"""
Load and transform patient characteristics and mutation data from Thompson et al..

Extracts the data from spreadsheets (extracted from the supporting information),
generates variant call format (VCF) files and outputs CSV files with harmonised patient
characteristics.

Thompson et al. "Serial Monitoring of Circulating Tumor DNA by Next-Generation Gene
Sequencing as a Biomarker of Response and Survival in Patients With Advanced NSCLC
Receiving Pembrolizumab-Based Therapy." JCO Precision Oncology 5 (2021): 510-524.
"""

from pathlib import Path

from numpy import nan
from pandas import read_excel
from pandas import DataFrame, concat, isnull

from ctdna.dataset.const import GUARDANT360_74_GENES
from ctdna.vcf.transform import CtdnaDataset


def _load_thompson_variants(
    excel_filename,
) -> DataFrame:
    """Load Thompson variant calls from spreadsheet and data cleanse."""
    X_variants = read_excel(excel_filename)
    X_variants = X_variants.set_index("Patient ID")
    X_variants["Variant Description"] = X_variants["Variant Description"].replace(
        {"Promoter region variant": "TF binding site variant"}
    )
    return X_variants


def set_time_weeks(X_variants, X_clinic):
    """Add timing of samples without variant calls."""
    # Add timing of follow-up samples.
    follow_ups = X_variants.index.unique().map(lambda x: f"{x}_1")
    missing_samples = set(follow_ups) - set(X_variants["Sample ID"])
    missing_samples = list(missing_samples)
    missing_ptids = [x.removesuffix("_1") for x in missing_samples]

    # Patients with no variants detected at both time points.
    ptid_non_shedder = sorted(set(X_clinic.index) - set(X_variants.index))
    sample_non_shedder = [x + "_1" for x in ptid_non_shedder]

    X_empty = DataFrame(
        {
            "Sample ID": missing_samples + sample_non_shedder,
            "Time (weeks)": 9,
            "Allele Fraction": 0.0,
        },
        columns=X_variants.columns,
        index=missing_ptids + ptid_non_shedder,
    )
    return concat([X_variants, X_empty])


def _parse_pdl1(value):
    ################
    # NB: all values containing per centage values are all > 70 %!
    if isinstance(value, str) and r"%" in value:
        return 1
    ##################
    elif isnull(value):
        return value
    return int(float(value) >= 0.5)


def _load_thompson_annotations(
    excel_filename,
) -> DataFrame:
    X = read_excel(excel_filename).set_index("Patient ID")

    X["stage_IV"] = (
        X.pop("stage").apply(lambda x: x.strip("abc")).replace({"IV": 1, "III": 0})
    )
    X["therapyline>1"] = X.pop("therapyline").apply(lambda x: 1 if int(x) > 1 else 0)
    X["smoker"] = X["smoker"].replace(
        {"former smoker": 1, "current smoker": 1, "never smoked": 0}
    )
    X["histology_squamous"] = X.pop("histology").apply(
        lambda x: 1 if x == "squamous cell carcinoma" else 0
    )

    X["pd_l1⩾50%"] = X.pop("PD-L1 expression").apply(_parse_pdl1)
    X["age_<65"] = X.pop("age")
    X["response_grouped"] = X["clinical_response"].replace(
        {
            "sd": "non responder (sd+pd+ne)",
            "pd": "non responder (sd+pd+ne)",
            "pr": "responder (pr+cr)",
            "cr": "responder (pr+cr)",
            "n/a": nan,
        }
    )
    X["therapy"] = X["therapy"].replace({"pembrolizumab": "αPD-1"})
    X["PS (ECOG) ≥ 2"] = X.pop("ECOG_PS").replace({0: 0, 1: 0, 2: 1})
    return X


def load_thompson_to_vcf(target_dir=Path("artifacts/vcf/thompson")):
    """Load variants and store as VCF on disk."""
    current_path = Path(__file__).parent.resolve()
    clinic_path = current_path / "raw/Thompson/clinical.xlsx"
    variant_path = current_path / "raw/Thompson/variants_20211011.xlsx"

    X_clinic = _load_thompson_annotations(clinic_path)
    X_variants = _load_thompson_variants(variant_path)
    X_variants = set_time_weeks(X_variants, X_clinic)

    pfs_resp = (X_clinic["pfs_months"] >= 6).astype(int)
    os_resp = (X_clinic["os_months"] >= 12).astype(int)

    # Check that labels are survival wise consistent.
    assert all(X_clinic["pfs_months"].round(3) <= X_clinic["os_months"].round(3))
    assert all(X_clinic[~pfs_resp.astype(bool)]["pfs_event"] == 1)

    dataset = CtdnaDataset(
        name="Thompson et al., JCO Precision Oncology (2021)",
        platform=GUARDANT360_74_GENES,
        reference_genome="hg19",
    )

    X_pfs = dataset.to_vcf(
        X_variants,
        pfs_resp,
        labels=["pfs<6months", "pfs≥6months"],
        target_dir=target_dir / "pfs",
    )
    X = concat([X_clinic, X_pfs], axis="columns")
    X["platform"] = GUARDANT360_74_GENES
    X.to_csv(target_dir / "pfs.csv")

    # assert all(X_clinic[~os_resp.astype(bool)]["os_event"] == 1)
    # dataset.to_vcf(
    #     X_variants,
    #     os_resp,
    #     labels=["os<1yr", "os≥1yr"],
    #     target_dir=target_dir / "os",
    # )
