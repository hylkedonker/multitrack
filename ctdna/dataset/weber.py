"""
Load and transform patient characteristics and mutation data from Weber et al..

Extracts the data from spreadsheets, generates variant call format (VCF) files and
outputs CSV files with harmonised patient characteristics.

Weber et al. "Dynamic Changes of Circulating Tumor DNA Predict Clinical Outcome in
Patients With Advanced Non–Small-Cell Lung Cancer Treated With Immune Checkpoint
Inhibitors." JCO precision oncology 5 (2021): 1540-1553.
"""

from pathlib import Path

from numpy import nan, timedelta64
from pandas import DataFrame, Series, concat, read_excel

from ctdna.dataset.const import AVENIO_EXPANDED_PANEL
from ctdna.vcf.transform import CtdnaDataset

PATIENT_ID_PREFIX = "WEBE"


def _load_weber_variants(
    excel_filename="raw/Weber/variant_list_20210204.xlsx",
) -> DataFrame:
    """Load Weber variant calls from spreadsheet and perform data cleansing."""
    X_variants = read_excel(excel_filename)
    X_variants["Patient ID"] = X_variants["Patient ID"].apply(
        lambda x: f"{PATIENT_ID_PREFIX}{int(x)}"
    )
    X_variants["Sample ID"] = X_variants["Sample ID"].apply(
        lambda x: f"{PATIENT_ID_PREFIX}{x}"
    )
    X_variants["Variant Description"] = X_variants["Variant Description"].replace(
        {"Disruptive inframe deletion": "Inframe deletion"}
    )
    return X_variants.set_index("Patient ID")


def to_lower(data: Series) -> Series:
    """Make text columns lowercase."""
    if hasattr(data, "str"):
        return data.str.lower()
    return data


def _transform_clinic(data: DataFrame) -> DataFrame:
    """
    Further curation of the clinical data.
    """

    X = data.copy().apply(to_lower)

    # Convert stage from float to integer.
    columns_to_int = ["stage", "therapyline"]
    X[columns_to_int] = X[columns_to_int].astype(int)

    # Unknown smoking status (=4) are probably smokers according Harry.
    # We group together current and previous (=2) smokers.
    X["smoker"] = X.pop("smokingstatus").replace({2: 1, 4: 1})

    meta_columns = [
        "lymfmeta",
        "brainmeta",
        "adrenalmeta",
        "livermeta",
        "lungmeta",
        "skeletonmeta",
    ]
    X["metastases"] = X[meta_columns].sum(axis=1).apply(lambda x: 1 if x > 0 else 0)
    X.drop(columns=meta_columns, inplace=True)

    X["pd_l1⩾50%"] = nan
    not_null = X["PD_L1_continous"].notnull()
    X.loc[not_null, "pd_l1⩾50%"] = (X.loc[not_null, "PD_L1_continous"] >= 50).astype(
        int
    )
    X.drop(columns="PD_L1_continous", inplace=True)

    X["stage_IV"] = X.pop("stage").replace({3: 0, 4: 1})

    X["therapy"] = X.pop("Systemischetherapie").replace(
        {
            "nivolumab + ipilimumab": "nivolumab+ipilimumab",
            "ipi-novu": "nivolumab+ipilimumab",
        }
    )
    X["therapy"] = X["therapy"].replace(
        {
            "nivolumab+ipilimumab": "αPD-1 + αCTLA-4",
            "nivolumab": "αPD-1",
            "pembrolizumab": "αPD-1",
            "atezolizumab": "αPD-L1",
            "durvalumab": "αPD-L1",
        }
    )

    # Curate clinical outcomes.
    X["response_grouped"] = X["response_grouped"].replace(
        {
            0: "non responder (sd+pd+ne)",
            2: "non responder (sd+pd+ne)",
            1: "responder (pr+cr)",
        }
    )
    X.rename(
        columns={
            "OS NEW": "os_months",
            "PFS_NEW": "pfs_months",
            "Censor_OS": "os_event",
            "Censor_progression": "pfs_event",
            "Clinical_Response.1": "clinical_response",
            "gender": "gender_male",
        },
        inplace=True,
    )

    # According to the selection criterea of the study, 0th and 1st line should be
    # combined.
    X["therapyline>1"] = (X.pop("therapyline") > 1).astype(int)

    # After inspection the "other" (=3) histologies are partially adeno.
    X["histology_squamous"] = X.pop("histology_grouped").replace({1: 0, 2: 1, 3: 0})
    X["age_<65"] = (X.pop("age") < 65).astype(int)
    X["PS (ECOG) ≥ 2"] = (X.pop("ECOG_PS") >= 2).astype(int)
    return X


def _load_weber_annotations(
    excel_filename="raw/Weber/clinical_20211015.xlsx",
) -> DataFrame:
    """Load Weber annotations from spreadsheet and perform data cleansing."""
    X_clinic = read_excel(excel_filename)

    # And set Patient ID as index.
    X_clinic.rename(columns={"studynumber": "Patient ID"}, inplace=True)
    X_clinic = X_clinic[X_clinic["Patient ID"].notna()]

    # The Weber et al. dataset consists of 167 patients.
    assert X_clinic.shape[0] == 167

    X_clinic.set_index(
        X_clinic["Patient ID"].map(lambda x: f"{PATIENT_ID_PREFIX}{int(x)}"),
        inplace=True,
    )
    columns = [
        "Clinical_Response.1",
        "response_grouped",
        "OS NEW",
        "Censor_OS",
        "PFS_NEW",
        "Censor_progression",
        "gender",
        "age",
        "stage",
        "therapyline",
        "smokingstatus",
        "Systemischetherapie",
        "histology_grouped",
        "lymfmeta",
        "brainmeta",
        "adrenalmeta",
        "livermeta",
        "lungmeta",
        "skeletonmeta",
        "ECOG_PS",
        "PD_L1_continous",
        "Date_BloodDraw_T1",
        "Syst.start",
    ]

    return _transform_clinic(X_clinic[columns])


def set_time_weeks(X_clin, X_var):
    """Extract sample timing and transfer to variant sheet."""
    week = timedelta64(1, "W")
    time_weeks = (X_clin["Date_BloodDraw_T1"] - X_clin["Syst.start"]) / week

    X_variants = X_var.copy()
    X_variants["Time (weeks)"] = 0
    # Set all follow up times according to `time_weeks`.
    for ptid, time in time_weeks.items():
        if ptid not in X_variants.index:
            # No variant call, but add record with timing.
            X_variants.loc[ptid, "Time (weeks)"] = time
            X_variants.loc[ptid, "Sample ID"] = f"{ptid}_1"
            X_variants.loc[ptid, "Allele Fraction"] = 0.0
            continue

        is_ptid = X_variants.index == ptid
        is_followup = X_variants["Sample ID"].apply(lambda x: x.endswith("_1"))
        x_followup = X_variants.loc[is_ptid & is_followup]
        if x_followup.size > 0:
            X_variants.loc[is_ptid & is_followup, "Time (weeks)"] = time
        else:
            X_empty = DataFrame(
                {
                    "Sample ID": f"{ptid}_1",
                    "Time (weeks)": time,
                    "Allele Fraction": 0.0,
                },
                columns=X_variants.columns,
                index=[ptid],
            )
            X_variants = concat([X_variants, X_empty])

    # Double check a few timings to make sure they are correct.
    is_followup = X_variants["Sample ID"].apply(lambda x: x.endswith("_1"))
    X_followups = X_variants[is_followup]
    assert (X_followups.loc["WEBE964", "Time (weeks)"] == 4.0).all()
    assert (X_followups.loc["WEBE1091", "Time (weeks)"] == 4.285714285714286).all()
    assert (X_followups.loc["WEBE7073", "Time (weeks)"] == 1.0).all()
    assert (X_followups.loc["WEBE7119", "Time (weeks)"] == 9.0).all()
    assert (X_variants[~is_followup]["Time (weeks)"] == 0.0).all()

    return X_variants


def load_weber_to_vcf(target_dir):
    """Load variants and store as VCF on disk."""
    current_path = Path(__file__).parent.resolve()
    clinic_path = current_path / "raw/Weber/clinical_20211015.xlsx"
    variant_path = current_path / "raw/Weber/variant_list_20210204.xlsx"

    X_clinic = _load_weber_annotations(clinic_path)
    X_variants = _load_weber_variants(variant_path)
    # Transfer sample timing to variant sheet.
    X_variants = set_time_weeks(X_clinic, X_variants)
    # After setting the timing, we no longer need these columns.
    X_clinic.drop(columns=["Date_BloodDraw_T1", "Syst.start"], inplace=True)

    pfs_resp = (X_clinic["pfs_months"] >= 6).astype(int)
    os_resp = (X_clinic["os_months"] >= 12).astype(int)

    # Check that labels are survival wise consistent.
    assert all(X_clinic["pfs_months"] <= X_clinic["os_months"])
    assert all(X_clinic[~pfs_resp.astype(bool)]["pfs_event"] == 1)
    assert all(X_clinic[~os_resp.astype(bool)]["os_event"] == 1)

    dataset = CtdnaDataset(
        name="Weber et al., JCO Precision Oncology (2021)",
        platform=AVENIO_EXPANDED_PANEL,
        reference_genome="hg38",  # GRCh38
    )

    X_pfs = dataset.to_vcf(
        X_variants,
        pfs_resp,
        labels=["pfs<6months", "pfs≥6months"],
        target_dir=target_dir / "pfs",
    )

    X = concat([X_clinic, X_pfs], axis="columns")
    X["platform"] = AVENIO_EXPANDED_PANEL
    X.to_csv(target_dir / "pfs.csv")

    X_os = dataset.to_vcf(
        X_variants,
        os_resp,
        labels=["os<1yr", "os≥1yr"],
        target_dir=target_dir / "os",
    )
    X = concat([X_clinic, X_os], axis="columns")
    X["platform"] = AVENIO_EXPANDED_PANEL
    X.to_csv(target_dir / "os.csv")
