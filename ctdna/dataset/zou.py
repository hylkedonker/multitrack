"""
Load and transform patient characteristics and mutation data from Zou et al..

Extracts the data from spreadsheets (obtained from the authors upon request), generates
variant call format (VCF) files and outputs CSV files with harmonised patient
characteristics.

Zou et al. "ctDNA Predicts Overall Survival in Patients With NSCLC Treated With PD-L1
Blockade or With Chemotherapy." JCO Precision Oncology 5 (2021): 827-838.
"""

from pathlib import Path

from pandas import concat, read_excel
from pandas.core.frame import DataFrame

from ctdna.dataset.const import AVENIO_SURVEILLANCE_PANEL
from ctdna.vcf.transform import CtdnaDataset


def percentage_to_float(x):
    if "%" in x:
        return float(x.removesuffix("%")) / 100
    return float(x)


def _load_zou_variants(
    excel_filename="raw/Zou/immuno/variants.xlsx",
) -> DataFrame:
    """Load Zou variant calls from spreadsheet and data cleanse."""
    X_variants = read_excel(excel_filename).set_index("Patient ID")
    X_variants["Allele Fraction"] = X_variants["Allele Fraction"].apply(
        percentage_to_float
    )
    X_variants["Variant Description"] = X_variants["Variant Description"].replace(
        {"5 prime UTR premature start codon gain variant": "5 prime UTR variant"}
    )
    return X_variants.rename(
        columns={
            "VARDEPTH": "Variant Depth",
            "TOTDEPTH": "Unique Depth",
        }
    )


def _load_zou_annotations(
    excel_filename="raw/Zou/immuno/clinical.xlsx",
) -> DataFrame:
    X = read_excel(excel_filename).set_index("Patient ID")
    X["stage_IV"] = X.pop("stage")  # All missing.

    # Each patient has had one one prior line of platinum-based treatment.
    X["therapyline>1"] = 1
    X["smoker"] = X["smoker"].replace(
        {"former smoker": 1, "current smoker": 1, "never smoked": 0}
    )
    X["histology_squamous"] = X.pop("histology").replace(
        {"Squamous": 1, "Non-Squamous": 0}
    )
    X["age_<65"] = X.pop("age").apply(lambda x: 1 if float(x) < 65 else 0)
    X["response_grouped"] = X["clinical_response"]  # Both missing.
    X["therapy"] = X["therapy"].replace({"atezolizumab": "αPD-L1"})
    X["PS (ECOG) ≥ 2"] = X.pop("ECOG_PS").replace({0: 0, 1: 0, 2: 1})
    return X.drop(
        columns=["race", "prior_therapyline"]
    )  # Too little datasets with this info.


def set_time_weeks(X_variants, X_clinic):
    """Add timing of samples without variant calls."""
    X_empty = DataFrame(
        {
            "Sample ID": ["ZOU5304_1"],
            "Time (weeks)": 3,
            "Allele Fraction": 0.0,
        },
        columns=X_variants.columns,
        index=["ZOU5304"],
    )

    # Shift time points 2 and 3 to 1 (since this was not observed).
    ZOU62017_2 = X_variants["Sample ID"] == "ZOU62017_2"
    ZOU62017_3 = X_variants["Sample ID"] == "ZOU62017_3"
    X_variants.loc[ZOU62017_2, "Sample ID"] = "ZOU62017_1"
    X_variants.loc[ZOU62017_3, "Sample ID"] = "ZOU62017_2"
    return concat([X_variants, X_empty])



def load_zou_chemo_to_vcf(target_dir=Path("artifacts/vcf/nsclc-chemo/zou")):
    """Load variants chemotherapy arm and store as VCF on disk."""
    current_path = Path(__file__).parent.resolve()
    clinic_path = current_path / "raw/Zou/chemo/clinical.xlsx"
    variant_path = current_path / "raw/Zou/chemo/variants.xlsx"

    X_clinic = _load_zou_annotations(clinic_path)
    X_variants = _load_zou_variants(variant_path)

    # Verify that nothing is missing.
    assert set(X_variants.index) == set(X_clinic.index)

    pfs_resp = (X_clinic["pfs_months"] >= 6).astype(int)
    os_resp = (X_clinic["os_months"] >= 12).astype(int)

    dataset = CtdnaDataset(
        name="Zou et al., JCO Precision Oncology (2021)",
        platform=AVENIO_SURVEILLANCE_PANEL,
        reference_genome="hg38",
    )

    X_pfs = dataset.to_vcf(
        X_variants,
        pfs_resp,
        labels=["pfs<6months", "pfs≥6months"],
        target_dir=target_dir / "pfs",
    )
    X = concat([X_clinic, X_pfs], axis="columns")
    X["platform"] = AVENIO_SURVEILLANCE_PANEL
    X.to_csv(target_dir / "pfs.csv")

    X_os = dataset.to_vcf(
        X_variants,
        os_resp,
        labels=["os<1yr", "os≥1yr"],
        target_dir=target_dir / "os",
    )
    X = concat([X_clinic, X_os], axis="columns")
    X["platform"] = AVENIO_SURVEILLANCE_PANEL
    X.to_csv(target_dir / "os.csv")


def load_zou_immuno_to_vcf(target_dir=Path("artifacts/vcf/nsclc-immunotherapy/zou")):
    """Load variants immunotherapy arm and store as VCF on disk."""
    current_path = Path(__file__).parent.resolve()
    clinic_path = current_path / "raw/Zou/immuno/clinical.xlsx"
    variant_path = current_path / "raw/Zou/immuno/variants.xlsx"

    X_clinic = _load_zou_annotations(clinic_path)
    X_variants = _load_zou_variants(variant_path)
    X_variants = set_time_weeks(X_variants, X_clinic)

    pfs_resp = (X_clinic["pfs_months"] >= 6).astype(int)
    os_resp = (X_clinic["os_months"] >= 12).astype(int)

    # Check that labels are survival wise consistent.
    assert all(X_clinic["pfs_months"] <= X_clinic["os_months"])
    assert all(X_clinic[~pfs_resp.astype(bool)]["pfs_event"] == 1)
    assert all(X_clinic[~os_resp.astype(bool)]["os_event"] == 1)

    dataset = CtdnaDataset(
        name="Zou et al., JCO Precision Oncology (2021)",
        platform=AVENIO_SURVEILLANCE_PANEL,
        reference_genome="hg38",
    )

    X_pfs = dataset.to_vcf(
        X_variants,
        pfs_resp,
        labels=["pfs<6months", "pfs≥6months"],
        target_dir=target_dir / "pfs",
    )
    X = concat([X_clinic, X_pfs], axis="columns")
    X["platform"] = AVENIO_SURVEILLANCE_PANEL
    X.to_csv(target_dir / "pfs.csv")

    X_os = dataset.to_vcf(
        X_variants,
        os_resp,
        labels=["os<1yr", "os≥1yr"],
        target_dir=target_dir / "os",
    )
    X = concat([X_clinic, X_os], axis="columns")
    X["platform"] = AVENIO_SURVEILLANCE_PANEL
    X.to_csv(target_dir / "os.csv")
