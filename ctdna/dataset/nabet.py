"""
Load and transform patient characteristics and mutation data from Nabet et al..

Extracts the data from spreadsheets (partly extracted from supporting information,
partly obtained upon request), generates variant call format (VCF) files and
outputs CSV files with harmonised patient characteristics.

Nabet et al.
"Noninvasive early identification of therapeutic benefit from immune checkpoint
inhibition."
Cell 183.2 (2020): 363-376.
"""

from pathlib import Path

from numpy import nan
from pandas import concat, read_excel
from pandas.core.frame import DataFrame

from ctdna.dataset.const import IDES_CAPP_SEQ
from ctdna.vcf.transform import CtdnaDataset


PATIENT_ID_PREFIX = "NABE"
MISSING_PATIENTS = [
    "NABE693",
    "NABE417",
    "NABE682",
    "NABE427",
    "NABE684",
    "NABE422",
    "NABE426",
    "NABE694",
    "NABE777",
    "NABE674",
    "NABE423",
    "NABE621",
    "NABE692",
    "NABE336",
    "NABE333",
    "NABE695",
]


def relabel_patientid(input_index):
    return PATIENT_ID_PREFIX + input_index.strip("LUP")


def _load_nabet_variants(
    excel_filename="../raw/Nabet/variants.xlsx",
) -> DataFrame:
    """Load Nabet variant calls from spreadsheet and data cleanse."""
    X_variants = read_excel(excel_filename)
    X_variants["Patient ID"] = X_variants["Patient ID"].apply(relabel_patientid)
    X_variants["Sample ID"] = X_variants["Sample ID"].apply(relabel_patientid)
    X_variants["Variant Description"] = X_variants["Variant Description"].replace(
        {
            "missense": "Missense variant",
            "synonymous": "Synonymous variant",
            "nonsense": "Stop gained",
        }
    )
    X_variants = X_variants.set_index("Patient ID")
    # We don't have clinical data for this sample.
    return X_variants.drop(index="NABE856")


def parse_pdl1_column(value):
    """Dichotomise PD-L1 expression value."""
    if str(value).lower() == "unknown":
        return nan
    elif float(value) >= 0.5:
        return 1
    return 0


def _load_nabet_annotations(
    excel_filename="../raw/Nabet/clinical.xlsx",
) -> DataFrame:
    """Load Nabet class labels, dropping records with missing ctDNA."""
    X = read_excel(excel_filename)
    X["Patient ID"] = X["Patient ID"].apply(relabel_patientid)
    X = X.set_index("Patient ID")

    print("Removing missing patients")
    print(MISSING_PATIENTS)
    X.drop(index=MISSING_PATIENTS, inplace=True)
    X["gender_male"] = X.pop("gender").str.lower().apply(lambda x: 1 if x == "m" else 0)
    X["histology_squamous"] = (
        X.pop("histology").str.lower().apply(lambda x: 1 if x == "squamous" else 0)
    )
    X["smoker"] = (
        X.pop("smokingstatus").str.lower().apply(lambda x: 0 if x == "never" else 1)
    )
    X["age_<65"] = X.pop("age").apply(lambda x: 1 if float(x) < 65 else 0)

    X["pd_l1⩾50%"] = X.pop("Tumor PD-L1 Expression").apply(parse_pdl1_column)
    X["stage_IV"] = 1

    # Those with `CNS = yes` have metastases at the central nervous sytem. For
    # others, we don't know if they have metastasised.
    X.loc[X["CNS Involvement Prior to ICI"] == "Yes", "metastases"] = 1

    X["therapyline>1"] = X.pop("Line of Therapy").apply(
        lambda x: 1 if int(x) > 1 else 0
    )

    # We are intersted in the first RECIST 1.1 response, not the best overall
    # response.
    X["clinical_response"] = nan
    X["response_grouped"] = X["clinical_response"].replace(
        {
            "sd": "non responder (sd+pd+ne)",
            "pd": "non responder (sd+pd+ne)",
            "pr": "responder (pr+cr)",
            "cr": "responder (pr+cr)",
        }
    )
    X["pfs>=6mths"] = (
        X.pop("Response").str.lower().apply(lambda x: 1 if x == "dcb" else 0)
    )
    X["PS (ECOG) ≥ 2"] = nan

    return X.drop(
        columns=[
            "Driver Mutation Status",
            "CNS Involvement Prior to ICI",
            "CNS Involvement at Progression",
            "Time (weeks)",
            "Best overall response",
        ]
    )


def load_nabet_to_vcf(target_dir=Path("artifacts/vcf/nabet")):
    """Load variants and store as VCF on disk."""
    current_path = Path(__file__).parent.resolve()
    clinic_path = current_path / "raw/Nabet/clinical.xlsx"
    variant_path = current_path / "raw/Nabet/variants.xlsx"

    X_clinic = _load_nabet_annotations(clinic_path)
    X_variants = _load_nabet_variants(variant_path)

    pfs_resp = X_clinic.pop("pfs>=6mths")

    dataset = CtdnaDataset(
        name="Nabet et al., Cell (2020)",
        platform=IDES_CAPP_SEQ,
        reference_genome="hg19",  # NCBI Build 37.1/GRCh37
    )

    pfs_labels = ["pfs<6months", "pfs≥6months"]
    X_pfs = dataset.to_vcf(
        X_variants,
        pfs_resp,
        labels=pfs_labels,
        target_dir=target_dir / "pfs",
    )
    X = concat([X_clinic, X_pfs], axis="columns")
    X["platform"] = IDES_CAPP_SEQ
    X.to_csv(target_dir / "pfs.csv")
