"""
Functions to build the combined dataset.

To build all the artifacts, execute the steps in the following order:
1) Generate VCF files and patient characteristics CSV's.
2) Split data in a train, development, and test set.
3) Use data of step 1) and filter out variants classified by ClinVar as non (likely)
    pathogenic.
4) Make a train, development, test split based on data of step 3) (which coincides with
   the split in step 2).
"""
from datetime import date
from pathlib import Path
from time import sleep
from typing import Optional
from urllib.error import HTTPError

from pandas import concat, read_csv
from pandas.core.frame import DataFrame
from sklearn.model_selection import train_test_split

from ctdna.dataset import get_artifacts_root, get_latest_artifact
from ctdna.dataset.anagnostou import load_anagnostou_to_vcf
from ctdna.dataset.nabet import load_nabet_to_vcf
from ctdna.dataset.thompson import load_thompson_to_vcf
from ctdna.dataset.weber import load_weber_to_vcf
from ctdna.dataset.zou import load_zou_chemo_to_vcf, load_zou_immuno_to_vcf
from ctdna.utils import line_count
from ctdna.vcf.annotate import apply_pathogenic_variant_filter


def step1_build_vcf_artifacts(target_dir=None):
    """Generate VCF files from spreadsheets."""
    today = date.today().strftime(r"%Y%m%d")
    if target_dir is None:
        target_dir = get_artifacts_root() / today / "1-vcf"

    # Primary analysis is immunotherapy treated non-small cell lung cancer.
    load_anagnostou_to_vcf(target_dir / "nsclc-immunotherapy" / "anagnostou")
    load_nabet_to_vcf(target_dir / "nsclc-immunotherapy" / "nabet")
    load_thompson_to_vcf(target_dir / "nsclc-immunotherapy" / "thompson")
    load_weber_to_vcf(target_dir / "nsclc-immunotherapy" / "weber")
    load_zou_immuno_to_vcf(target_dir / "nsclc-immunotherapy" / "zou")

    # Secondary analysis is on chemotherapy treated non-small cell lung cancer.
    load_zou_chemo_to_vcf(target_dir / "nsclc-chemo" / "zou")


def step2_build_train_dev_test_split(
    pattern: str = "*/pfs.csv", source_dir=None, target_dir=None
) -> tuple[DataFrame]:
    """Make stratified train-dev-test sets of immunotherapy set."""
    today = date.today().strftime(r"%Y%m%d")

    if source_dir is None:
        source_dir = get_artifacts_root() / today / "1-vcf" / "nsclc-immunotherapy"
    else:
        source_dir = source_dir / "nsclc-immunotherapy"
    if target_dir is None:
        target_dir = get_artifacts_root() / today / "2-split" / "nsclc-immunotherapy"
    else:
        target_dir = target_dir / "nsclc-immunotherapy"

    X = []
    for csv_path in source_dir.glob(pattern):
        if not csv_path.is_file():
            continue
        dataset = csv_path.parent.name
        X_sub = read_csv(csv_path, index_col=0)
        X_sub["strata"] = str(dataset) + ":" + X_sub["class"]
        X.append(X_sub)

    X_all = concat(X, axis="rows").sort_index()

    # Make a target label and dataset stratified 60-20-20 % split.
    X_train, X_devtest = train_test_split(
        X_all, train_size=0.6, stratify=X_all["strata"], random_state=1234
    )
    X_dev, X_test = train_test_split(
        X_devtest, train_size=0.5, stratify=X_devtest["strata"], random_state=1234
    )

    X_train = X_train.drop(columns="strata")
    X_dev = X_dev.drop(columns="strata")
    X_test = X_test.drop(columns="strata")

    target_dir.mkdir(parents=True, exist_ok=True)
    X_all.drop(columns="strata").to_csv(target_dir / "all.csv")
    X_train.to_csv(target_dir / "train.csv")
    X_dev.to_csv(target_dir / "dev.csv")
    X_test.to_csv(target_dir / "test.csv")

    return X_train, X_dev, X_test


def _build_annotated_vcf_artifacts(source_dir: Path, destination_dir: Path):
    """Find VCF files and annotate variants contained in them."""
    for source_vcf in source_dir.rglob("*.vcf"):
        destination_vcf = destination_dir / source_vcf.relative_to(
            get_latest_artifact() / "1-vcf" / "nsclc-immunotherapy"
        )
        print(destination_vcf)
        destination_vcf.parent.mkdir(exist_ok=True, parents=True)

        try:
            apply_pathogenic_variant_filter(source_vcf, destination_vcf)
        # Retry upon failing.
        except HTTPError:
            print("Retrying in 10 secs.")
            sleep(10)
            apply_pathogenic_variant_filter(source_vcf, destination_vcf)

        # Only FILTER field and header should have been altered.
        assert line_count(source_vcf, comments=False) == line_count(
            destination_vcf, comments=False
        )


def step3_build_annotated_vcf_artifacts(source_dir: Optional[Path] = None):
    """Annotate using ClinVar and filter out non-pathogenic variants."""
    if source_dir is None:
        source_dir = get_latest_artifact() / "1-vcf" / "nsclc-immunotherapy"

    # Only do this for the immunotherapy set.
    target_dir = source_dir.parent.parent / "3-vcf-pathogenic" / "nsclc-immunotherapy"
    _build_annotated_vcf_artifacts(source_dir / "anagnostou" / "pfs", target_dir)
    _build_annotated_vcf_artifacts(source_dir / "nabet" / "pfs", target_dir)
    _build_annotated_vcf_artifacts(source_dir / "thompson" / "pfs", target_dir)
    _build_annotated_vcf_artifacts(source_dir / "weber" / "pfs", target_dir)
    _build_annotated_vcf_artifacts(source_dir / "zou" / "pfs", target_dir)


def step4_build_pathogenic_split():
    """Build train-dev-test splits for pathogenic variants."""
    root = get_latest_artifact()
    target_dir = root / "4-split" / "nsclc-immunotherapy"
    target_dir.mkdir(parents=True, exist_ok=True)
    # Update directory in original files and write to new destination.
    for csv_source in (root / "2-split" / "nsclc-immunotherapy").glob("*.csv"):
        csv_target = target_dir / csv_source.name
        df = read_csv(csv_source, index_col=0)
        df["directory"] = df["directory"].str.replace("1-vcf", "3-vcf-pathogenic")
        df.to_csv(csv_target)
