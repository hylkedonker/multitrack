"""
To build the circulating tumor DNA and patient characteristics artifacts (which
is placed in the artifacts subfolder) from the spreadsheets (located under
./ctdna/datasets/raw), execute the following:


$ step1_build_vcf_artifacts()
$ step2_build_train_dev_test_split()

Optional: Filter the variants based on whether they were indicated as (likely)
pathogenic (analysis in Supplementary Material):

$ step3_build_annotated_vcf_artifacts()
$ step4_build_pathogenic_split()
"""
from datetime import datetime
from pathlib import Path
from typing import Union

from pandas import DataFrame, read_csv


def get_artifacts_root() -> Path:
    """Resolve default artifacts root directory."""
    artifact_root = Path(__file__).parent.resolve() / "../../artifacts"
    return artifact_root.resolve()


def get_latest_artifact() -> Path:
    """Determine latest artifact path by date."""
    root = get_artifacts_root()

    artifacts = {}
    for child in root.iterdir():
        if not child.is_dir():
            continue
        try:
            artifact_date = datetime.strptime(child.name, r"%Y%m%d")
        except ValueError:
            continue
        else:
            artifacts[artifact_date] = child

    # Select most recent artifacts build.
    latest_date = sorted(artifacts.keys())[-1]
    latest_path = artifacts[latest_date]
    return latest_path


def load_splits(all: bool = False, step: int = 2) -> Union[DataFrame, tuple[DataFrame]]:
    """Load latest immunotherapy artifacts.

    Args:
        all: Entire pooled dataset (True) or separate train-dev-test splits (False).
        step: Use split directly after generating VCF's from spreadsheets (=step 2) or
            after subsequent filtering by pathogenicity (=step 4)
    """
    artifact_path = get_latest_artifact()
    splits_path = artifact_path / f"{step}-split" / "nsclc-immunotherapy"
    if all:
        return read_csv(splits_path / "all.csv", index_col=["Patient ID", "platform"])

    df_train = read_csv(splits_path / "train.csv", index_col=["Patient ID", "platform"])
    df_dev = read_csv(splits_path / "dev.csv", index_col=["Patient ID", "platform"])
    df_test = read_csv(splits_path / "test.csv", index_col=["Patient ID", "platform"])
    return df_train, df_dev, df_test
