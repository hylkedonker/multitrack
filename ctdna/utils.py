from pathlib import Path
from typing import Iterable

from numpy import sqrt
from pandas import DataFrame, Series
from sklearn.metrics import roc_auc_score

from ctdna.statistics import bootstrap_score


def stratify_by_dataset(y_pred) -> dict:
    """Extract and split by dataset using index."""
    data_slices = {"overall": y_pred}
    dataset = y_pred.index.map(lambda x: x[:3])

    anagnostou = y_pred.loc[dataset == "ANA"]
    if anagnostou.size > 0:
        data_slices["Anagnostou et al. ('19)"] = anagnostou

    nabet = y_pred.loc[dataset == "NAB"]
    if nabet.size > 0:
        data_slices["Nabet et al. ('20)"] = nabet

    thompson = y_pred.loc[dataset == "THO"]
    if thompson.size > 0:
        data_slices["Thompson et al. ('21)"] = thompson

    weber = y_pred.loc[dataset == "WEB"]
    if weber.size > 0:
        data_slices["Weber et al. ('21)"] = weber

    zou = y_pred.loc[dataset == "ZOU"]
    if zou.size > 0:
        data_slices["Zou et al. ('21)"] = zou

    return data_slices


def _roc_auc_confidence_interval(n1: int, n2: int, auc: float) -> tuple[float]:
    """
    Ref:
    Hanley, James A., and Barbara J. McNeil. "The meaning and use of the area
    under a receiver operating characteristic (ROC) curve." Radiology 143.1
    (1982): 29-36.
    """
    q0 = auc * (1 - auc)
    q1 = auc / (2 - auc)
    q2 = 2 * auc ** 2 / (1 + auc)
    sigma = sqrt(q0 + (n1 - 1) * (q1 - auc ** 2) + (n2 - 1) * (q2 - auc ** 2)) / sqrt(
        n2 * n1
    )
    return auc - 1.960 * sigma, auc + 1.960 * sigma


def asymptotic_roc_auc_confidence_interval(y_true, y_pred) -> tuple[float]:
    """Compute ROC AUC with 95 % confidence interval."""
    auc = roc_auc_score(y_true, y_pred)
    n2 = y_true.sum()
    n1 = len(y_true) - n2
    lower, upper = _roc_auc_confidence_interval(n1, n2, auc)
    return auc, lower, upper


def stratified_roc_auc(y_true, y_pred):
    """Compute asymptotic ROC AUC CI, stratified by dataset."""
    if not isinstance(y_pred, Series) and isinstance(y_true, Series):
        y_pred = Series(y_pred, index=y_true.index)

    data_subsets = stratify_by_dataset(y_pred)
    scores = DataFrame(
        index=list(data_subsets.keys()),
        columns=["point", "lower", "upper"],
    )

    for name, y_slice in data_subsets.items():

        auc, auc_lower, auc_upper = asymptotic_roc_auc_confidence_interval(
            y_true=y_true.loc[y_slice.index], y_pred=y_slice
        )
        score = {
            "point": auc,
            "lower": auc_lower,
            "upper": auc_upper,
        }
        scores.loc[name] = score
    return scores


def stratified_score(y_true, y_pred, metric, bootstrap=True):
    """Compute score stratified by dataset."""
    if not isinstance(y_pred, Series) and isinstance(y_true, Series):
        y_pred = Series(y_pred, index=y_true.index)

    data_subsets = stratify_by_dataset(y_pred)
    scores = DataFrame(
        index=list(data_subsets.keys()),
        columns=["point", "lower", "upper"],
    )

    for name, y_slice in data_subsets.items():
        if bootstrap:
            score = bootstrap_score(
                y_true=y_true[y_slice.index],
                y_pred=y_slice,
                metric=metric,
            )
        else:
            score = metric(y_true[y_slice.index], y_slice)
        scores.loc[name] = score
    return scores


def unpack_variants(variant_types: Iterable) -> set:
    """Extract individual variants seperated by a "&"."""
    unique_variants = []
    for variant in variant_types:
        if variant == "":
            continue
        elif "&" in variant:
            unique_variants.extend(variant.split("&"))
        else:
            unique_variants.append(variant)
    return set(unique_variants)


def line_count(vcf_file: Path, comments: bool = False) -> int:
    """Count number of non-comment lines in VCF file."""
    with open(vcf_file) as fo:
        return len(
            [line for line in fo.readlines() if comments or not line.startswith("#")]
        )
