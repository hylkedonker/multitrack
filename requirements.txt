numpy>=1.20.0

## The following packages have conflicting NumPy dependencies. So instead, we
# directly put them in the Dockerfile.
pomegranate>=0.14.8
shap

optuna
scikit-learn>=1.0
openpyxl
pandas
vcfpy
coverage
pysam
ipdb
dill
seaborn
biopython
