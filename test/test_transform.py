from unittest import TestCase

from numpy import nan
from ctdna.vcf.transform import (
    is_number,
    parse_genomic_position,
    abbreviate_amino_acid,
)


class TestParse(TestCase):
    """Test various input string parsings."""

    def test_genomic_position(self):
        """Test various input for genomic position."""
        # 1) Only start position.
        test_str = "chr7:5987371"
        self.assertEqual(
            parse_genomic_position(test_str),
            ("chr7", 5987371, None),
        )

        # 2) Position range (start to end).
        test_str = "chr17:7577542-7577550"
        self.assertEqual(
            parse_genomic_position(test_str),
            ("chr17", 7577542, 7577550),
        )

        # 3) Sex chromosome.
        test_str = "chrX:7577542-7577550"
        self.assertEqual(
            parse_genomic_position(test_str),
            ("chrX", 7577542, 7577550),
        )

        # 4) Unknown position (for instance, in Thompson et al. dataset).
        test_str = "chr7:?(E736K)"
        self.assertEqual(parse_genomic_position(test_str), ("chr7", None, None))

    def test_to_amino_acid_symbol(self):
        """Test that amino acid symbol are correctly abbreviated."""
        self.assertEqual(abbreviate_amino_acid("p.Lys465Arg"), "p.K465R")

        # Check that all matches are mapped.
        self.assertEqual(abbreviate_amino_acid("p.His321His"), "p.H321H")

        # Test identify mapping for strings not needing alteration.
        self.assertEqual(abbreviate_amino_acid("H321H"), "H321H")

        # Correct frame shift notation.
        self.assertEqual(abbreviate_amino_acid("D48*"), "D48*")
        # Incorrect frame shift notation, but we shall allow it.
        self.assertEqual(abbreviate_amino_acid("D48fs"), "D48fs")

        # Don't do anything for missing values.
        self.assertTrue(abbreviate_amino_acid(nan) is nan)

    def test_is_number(self):
        """Test for correct sequencing depth number parsing."""
        self.assertTrue(is_number("56.0"))
        self.assertFalse(is_number("."))
        self.assertFalse(is_number(nan))
