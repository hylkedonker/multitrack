from pathlib import Path
from tempfile import mkdtemp
import unittest
from unittest.case import TestCase

from numpy import nan
from numpy.testing import assert_array_equal
import pandas as pd
from pandas import DataFrame, NA, Series

from ctdna.vcf.load import load_vcf, load_vcf_dir
from ctdna.vcf.transform import CtdnaDataset, SampleMetaData, to_vcf
from test.factory import fabricate_variant_calls


class TestSave(unittest.TestCase):
    def setUp(self):
        """Fabricate some data to save on disk."""
        self.variants_t0, self.variants_t1, self.variants_t2 = fabricate_variant_calls()

    def num_lines(self, file_path):
        """Count how many lines are in `file_path` not starting with a "#"."""
        with open(file_path) as fo:
            records = [line for line in fo.readlines() if not line.startswith("#")]
            return len(records)

    def test_to_vcf(self):
        """Test that VCF is created."""
        target_dir = Path(mkdtemp())
        meta = SampleMetaData(
            dataset="test",
            reference_genome="test",
            time_point=0,
            platform="test",
            patient_id="WEBE1051",
        )
        to_vcf(self.variants_t0, meta, output_dir=target_dir)

        target_file = target_dir / "WEBE1051/t0.vcf"
        # Check that the file has been created.
        self.assertTrue(target_file.exists())
        self.assertEqual(self.num_lines(target_file), 3)

    def test_save(self):
        """Test saving a variant sheet to VCF."""
        data = CtdnaDataset(name="test", platform="test", reference_genome="?")
        y = Series([0], index=["WEBE1051"])

        # 1)
        # Only variant calls at t0.
        target_dir = Path(mkdtemp())
        data.to_vcf(self.variants_t0, y, target_dir=target_dir)
        target = target_dir / "0" / "WEBE1051"

        # Three variants at t0.
        target_t0 = target / "t0.vcf"
        self.assertTrue(target_t0.exists())
        self.assertEqual(self.num_lines(target_t0), 3)

        # No variants at t1.
        target_t1 = target / "t1.vcf"
        self.assertTrue(target_t1.exists())
        self.assertEqual(self.num_lines(target_t1), 0)

        # 2)
        # Only variant calls at t1.
        target_dir = Path(mkdtemp())
        data.to_vcf(self.variants_t1, y, target_dir=target_dir)
        target = target_dir / "0" / "WEBE1051"

        # No variants at t0.
        target_t0 = target / "t0.vcf"
        self.assertTrue(target_t0.exists())
        self.assertEqual(self.num_lines(target_t0), 0)

        # Three variants at t1.
        target_t1 = target / "t1.vcf"
        self.assertTrue(target_t1.exists())
        self.assertEqual(self.num_lines(target_t1), 3)

        # 3)
        # Variant calls at t0 and t1.
        target_dir = Path(mkdtemp())
        X = pd.concat([self.variants_t0, self.variants_t1])
        data.to_vcf(X, y, target_dir=target_dir)
        target = target_dir / "0" / "WEBE1051"

        # Three variants at t0.
        target_t0 = target / "t0.vcf"
        self.assertTrue(target_t0.exists())
        self.assertEqual(self.num_lines(target_t0), 3)

        # Three variants at t1.
        target_t1 = target / "t1.vcf"
        self.assertTrue(target_t1.exists())
        self.assertEqual(self.num_lines(target_t1), 3)

        # 4)
        # Variants calls at t0 and t2.
        target_dir = Path(mkdtemp())
        X = pd.concat([self.variants_t0, self.variants_t2])
        data.to_vcf(X, y, target_dir=target_dir)
        target = target_dir / "0" / "WEBE1051"

        # Three variants at t0.
        target_t0 = target / "t0.vcf"
        self.assertTrue(target_t0.exists())
        self.assertEqual(self.num_lines(target_t0), 3)

        # No variants at t1.
        target_t1 = target / "t1.vcf"
        self.assertTrue(target_t1.exists())
        self.assertEqual(self.num_lines(target_t1), 0)

        # One variant at t2.
        target_t2 = target / "t2.vcf"
        self.assertTrue(target_t2.exists())
        self.assertEqual(self.num_lines(target_t2), 1)

    def test_save_non_shedder(self):
        """Test saving patient with no variant calls at t0 + t1."""
        data = CtdnaDataset(name="test", platform="test", reference_genome="?")
        y = Series([0], index=["WEBE1051"])

        target_dir = Path(mkdtemp())
        empty = DataFrame(columns=self.variants_t0.columns)
        data.to_vcf(empty, y, target_dir=target_dir)
        target = target_dir / "0" / "WEBE1051"

        # Check that VCF are created for both files.
        target_t0 = target / "t0.vcf"
        target_t1 = target / "t1.vcf"
        self.assertTrue(target_t0.exists())
        self.assertTrue(target_t1.exists())


class TestLoad(TestCase):
    def setUp(self):
        """Fabricate some data to save on disk."""
        self.variants_t0, self.variants_t1, self.variants_t2 = fabricate_variant_calls()

    def save_and_load(self, variants: DataFrame) -> DataFrame:
        """Store variants to VCF  load back from disk."""
        data = CtdnaDataset(name="test", platform="test", reference_genome="?")
        y = Series([0], index=["WEBE1051"])

        target_dir = Path(mkdtemp())
        data.to_vcf(variants, y, target_dir=target_dir)
        return load_vcf_dir(target_dir / "0" / "WEBE1051")

    def assert_gene_allele_fraction(
        self, original: DataFrame, loaded: DataFrame, time_point
    ):
        """Assert matching  gene and allele fraction pairs."""
        expected = original.loc[["WEBE1051"]].drop(columns="Time (weeks)")
        expected = expected[["Gene", "Allele Fraction"]].set_index("Gene")
        actual = loaded.reset_index()[["gene", time_point]].set_index("gene")
        assert_array_equal(
            expected.sort_index().to_numpy(),
            actual.sort_index().to_numpy(),
        )

    def test_load_clearance(self):
        """Test loading patient with no variant calls at t0 + t1."""
        empty = DataFrame(columns=self.variants_t0.columns)
        variants = self.save_and_load(empty)
        # No variant calls at t0 and t1 is intepreted as 0.0.
        assert_array_equal(variants, [[0.0, 0.0]])

    def test_load(self):
        """Test loading a variant sheet from disk."""
        # 1)
        # Only variant calls at t0.
        variants = self.save_and_load(self.variants_t0)

        # No variants at t1.
        self.assertTrue((variants["t1"] == 0).all().all())
        # And the variants at t0 should match.
        self.assert_gene_allele_fraction(self.variants_t0, variants, time_point="t0")

        # 2)
        # Only variant calls at t1.
        variants = self.save_and_load(self.variants_t1)
        # No variants at t1.
        self.assertTrue((variants["t0"] == 0).all().all())
        # And the variants at t0 should match.
        self.assert_gene_allele_fraction(self.variants_t1, variants, time_point="t1")

        # 3)
        # Variant calls at t0 and t1.
        X = pd.concat([self.variants_t0, self.variants_t1])
        variants = self.save_and_load(X)
        # Forget about time_weeks level.
        variants = variants.droplevel(level="time_weeks", axis="columns")

        # When we select all non-zero VAF's, we should recover the original
        # variant calls.
        variants_t0 = variants["t0"]
        reconstr_t0_variants = variants_t0[variants_t0 != 0.0]
        self.assert_gene_allele_fraction(
            self.variants_t0, reconstr_t0_variants, time_point="t0"
        )
        variants_t1 = variants["t1"]
        reconstr_t1_variants = variants_t1[variants_t1 != 0.0]
        self.assert_gene_allele_fraction(
            self.variants_t1, reconstr_t1_variants, time_point="t1"
        )

        # 3)
        # Variant calls at t0 and t2.
        X = pd.concat([self.variants_t0, self.variants_t2])
        variants = self.save_and_load(X)
        variants = variants.droplevel(level="time_weeks", axis="columns")

        # When we select all non-zero VAF's, we should recover the original
        # variant calls.
        reconstr_t0_variants = variants[variants["t0"] != 0.0]
        self.assert_gene_allele_fraction(
            self.variants_t0, reconstr_t0_variants, time_point="t0"
        )

        self.assertTrue((variants["t1"] == 0).all())
        reconstr_t2_variants = variants[variants["t2"] != 0.0]
        self.assert_gene_allele_fraction(
            self.variants_t2, reconstr_t2_variants, time_point="t2"
        )

    def test_load_no_metadata_vcf(self):
        """Test AVENIO VCF data without `patient_id` and `platform` headers."""
        vcf_dir = Path(__file__).parent.resolve() / "dataset" / "lib1"
        filename = vcf_dir / "SampleEfIS_ROC2_ST10_50ng_LIB1.vcf"
        vafs = load_vcf(filename)
        record = {
            "time_point": "sampleefis_roc2_st10_50ng_lib1",
            "time_weeks": NA,
            "chromosome": "chrX",
            "position": 67545785,
            "gene": "AR",
            "variant_type": "synonymous_variant",
            "coding_change": "c.639G>A",
            "amino_acid_change": "p.Glu213Glu",
        }
        record_allele_frequency = 0.00900901
        idx = tuple(record.values())
        self.assertTrue(idx in vafs.index)
        self.assertEqual(vafs.loc[idx], record_allele_frequency)

    def test_load_ensemble_vep(self):
        """Test load an unannotated VCF file that has been submited to VEP."""
        vcf_dir = Path(__file__).parent.resolve() / "dataset"
        filename = vcf_dir / "igsr-vep.vcf"
        vafs = load_vcf(filename)

        record = {
            "time_point": "igsr-vep",
            "time_weeks": nan,
            "chromosome": "20",
            "position": 1110696,
            "gene": "PSMF1",
            "variant_type": "upstream_gene_variant",
            "coding_change": "",
            "amino_acid_change": "",
        }
        record_allele_frequency = 0.333
        idx = tuple(record.values())
        self.assertTrue(idx in vafs.index)
        self.assertEqual(vafs.loc[idx].iloc[0], record_allele_frequency)
