from unittest import TestCase

from numpy.testing import assert_almost_equal
from ctdna.utils import _roc_auc_confidence_interval


class TestConfidenceInterval(TestCase):
    def test_confidence_interval(self):
        """Check computation with known outcome."""
        lower, upper = _roc_auc_confidence_interval(n1=527, n2=279, auc=0.88915)
        assert_almost_equal(lower, 0.867363, decimal=5)
        assert_almost_equal(upper, 0.910937, decimal=5)
