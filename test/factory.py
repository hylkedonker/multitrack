from pandas import DataFrame
from numpy import array


def fabricate_variant_calls(
    name: str = "WEBE1051",
) -> tuple[DataFrame, DataFrame]:
    """Fabricate some data to save on disk."""
    # Variant calls at baseline (t0).
    array_t0 = array(
        [
            (
                name,
                f"{name}_0",
                0.0,
                "ABL1",
                "ENST00000372348.6",
                "c.678C>T",
                "Synonymous variant",
                0.0339,
                "chr9:130862834",
            ),
            (
                name,
                f"{name}_0",
                0.0,
                "TERT",
                "ENST00000310581.9",
                "c.-104T>C",
                "Upstream gene variant",
                0.015,
                "chr5:1295093",
            ),
            (
                name,
                f"{name}_0",
                0.0,
                "TP53",
                "ENST00000269305.8",
                "c.461G>T",
                "Missense variant",
                0.0114,
                "chr17:7675151",
            ),
        ],
        dtype=[
            ("Patient ID", "O"),
            ("Sample ID", "O"),
            ("Time (weeks)", "O"),
            ("Gene", "O"),
            ("Transcript", "O"),
            ("Coding Change", "O"),
            ("Variant Description", "O"),
            ("Allele Fraction", "<f8"),
            ("Genomic Position", "O"),
        ],
    )
    # Variant calls at follow-up (t1).
    array_t1 = array(
        [
            (
                name,
                f"{name}_1",
                2.5,
                "ABL1",
                "ENST00000372348.6",
                "c.678C>T",
                "Synonymous variant",
                0.0915,
                "chr9:130862834",
            ),
            (
                name,
                f"{name}_1",
                2.5,
                "MET",
                "ENST00000318493.10",
                "c.378T>C",
                "Synonymous variant",
                0.0217,
                "chr7:116699462",
            ),
            (
                name,
                f"{name}_1",
                2.5,
                "TERT",
                "ENST00000310581.9",
                "c.-104T>C",
                "Upstream gene variant",
                0.02,
                "chr5:1295093",
            ),
        ],
        dtype=[
            ("Patient ID", "O"),
            ("Sample ID", "O"),
            ("Time (weeks)", "O"),
            ("Gene", "O"),
            ("Transcript", "O"),
            ("Coding Change", "O"),
            ("Variant Description", "O"),
            ("Allele Fraction", "<f8"),
            ("Genomic Position", "O"),
        ],
    )
    # Variant calls at follow-up (t2).
    array_t2 = array(
        [
            (
                name,
                f"{name}_2",
                5.0,
                "KEAP1",
                "ENST00000393623.6",
                "c.1708+2G>T",
                "Splice donor variant & Intron variant",
                0.1,
                "chr19:10489192",
            ),
        ],
        dtype=[
            ("Patient ID", "O"),
            ("Sample ID", "O"),
            ("Time (weeks)", "O"),
            ("Gene", "O"),
            ("Transcript", "O"),
            ("Coding Change", "O"),
            ("Variant Description", "O"),
            ("Allele Fraction", "<f8"),
            ("Genomic Position", "O"),
        ],
    )
    variants_t0 = DataFrame.from_records(array_t0, index="Patient ID")
    variants_t1 = DataFrame.from_records(array_t1, index="Patient ID")
    variants_t2 = DataFrame.from_records(array_t2, index="Patient ID")
    return variants_t0, variants_t1, variants_t2
