from unittest import TestCase

from ctdna.vcf.annotate import is_pathogenic_protein_change, is_pathogenic_variant


class TestPathogenicAnnotation(TestCase):
    """Test that mutations are correctly identified as (likely) pathogenic."""

    def test_variant_annotations(self):
        """Test annotation by coordinates.."""
        self.assertTrue(
            is_pathogenic_variant(
                "chr17", 39724728, assembly="hg38", allow_drug_response=False
            )
        )
        self.assertTrue(
            is_pathogenic_variant(
                "chr12", 25245351, assembly="hg38", allow_drug_response=True
            )
        )
        self.assertTrue(
            is_pathogenic_variant(
                "chr17", 7675139, assembly="hg38", allow_drug_response=True
            )
        )
        self.assertTrue(
            is_pathogenic_variant(
                "chr2", 178098804, assembly="hg19", allow_drug_response=True
            )
        )
        self.assertTrue(
            is_pathogenic_variant(
                "chr7",
                55174772,
                end_pos=55174786,
                assembly="hg38",
                allow_drug_response=True,
            )
        )
        self.assertFalse(
            is_pathogenic_variant("chr5", 180629400, assembly="hg38"),
        )
        self.assertFalse(
            is_pathogenic_variant(
                "chr2",
                51255096,
                end_pos=51255096,
                assembly="hg19",
                allow_drug_response=True,
            )
        )
        self.assertTrue(
            is_pathogenic_variant("chr7", 55242468, allow_drug_response=True)
        )
        # q=chr13:48941720
        self.assertTrue(
            is_pathogenic_variant(
                "chr13", 48941720, assembly="hg19", allow_drug_response=True
            )
        )

    def test_protein_annotation(self):
        """Test annotation by protein change."""
        self.assertTrue(
            is_pathogenic_protein_change("BRAF", "V600E", allow_drug_response=True)
        )
        self.assertFalse(
            is_pathogenic_protein_change("EGFR", "E736K", allow_drug_response=True)
        )
        self.assertTrue(
            is_pathogenic_protein_change("KRAS", "G12V", allow_drug_response=True)
        )
        self.assertFalse(
            is_pathogenic_protein_change("TP53", "Q136*", allow_drug_response=True)
        )
        self.assertFalse(
            is_pathogenic_protein_change("ARID1A", "M1564fs", allow_drug_response=True)
        )
        self.assertFalse(
            is_pathogenic_protein_change("MAPK1", "E322*", allow_drug_response=True)
        )
        self.assertTrue(
            is_pathogenic_protein_change("BRCA1", "E1694*", allow_drug_response=True)
        )
        self.assertTrue(
            is_pathogenic_protein_change("TP53", "R213fs", allow_drug_response=True)
        )
        self.assertFalse(is_pathogenic_protein_change("NF1", "Y2640C"))

    def test_ill_posed_protein_change(self):
        """Test that ill posed protein changes cause errors."""
        with self.assertRaises(KeyError):
            is_pathogenic_protein_change("TERT", "Promoter mut")
        with self.assertRaises(KeyError):
            is_pathogenic_protein_change("STK11", "Splice Var")
