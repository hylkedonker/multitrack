from collections.abc import Iterable
from pathlib import Path
from shutil import rmtree
from tempfile import TemporaryDirectory, mkdtemp
from unittest import TestCase, skip

from numpy import inf
from numpy.testing import assert_array_equal
from numpy.testing._private.utils import assert_almost_equal
from pandas import Series, concat, read_csv
from pandas.core.frame import DataFrame
import vcfpy

from ctdna.dataset.const import (
    ASSAY_SIZE,
    AVENIO_EXPANDED_PANEL,
    AVENIO_SURVEILLANCE_PANEL,
    GUARDANT360_74_GENES,
    IDES_CAPP_SEQ,
    TEC_SEQ,
    TARGET_CHROMS,
    VARIANT_VOCABULARY,
)
from ctdna.dataset.build import (
    step2_build_train_dev_test_split,
    step1_build_vcf_artifacts,
)
from ctdna.utils import line_count, unpack_variants
from ctdna.vcf.annotate import apply_pathogenic_variant_filter
from ctdna.vcf.preprocessing import (
    VcfDataGenerator,
    chromosome_layer,
    count_mutations,
    count_ratio,
    filter_not_called,
    gene_ratio_layer,
    molecular_response_layer,
    molecular_response_layer_old,
    total_vaf,
)
from ctdna.vcf.transform import CtdnaDataset
from test.factory import fabricate_variant_calls


class TestBaseVcfDataGenerator(TestCase):
    def generate_vcf(self, target_dir, name, platform, time_points=(0, 1), label=1):
        """Generate VCF files for made up variant calls."""
        # Which fabricated time points (0, 1, 2) to use is determined by
        # `time_points`.
        variants_at_t = fabricate_variant_calls(name)

        if time_points:
            variants = concat([variants_at_t[i] for i in time_points])
        else:  # Empty `time_points` -> empty frame of variants.
            variants = DataFrame(columns=variants_at_t[0].columns)

        # Write to VCF.
        data = CtdnaDataset(name="test", platform=platform, reference_genome="?")
        y = Series([label], index=[name])
        data.to_vcf(variants, y, target_dir=target_dir)
        return variants_at_t

    def setUp(self):
        """Generate mock VCF."""
        # Assume no variant calls at follow up.
        self.target_dir = Path(mkdtemp())
        self.dummy1_var = self.generate_vcf(
            self.target_dir,
            name="dummy1",
            platform=AVENIO_EXPANDED_PANEL,
            time_points=(0,),
            label=0,
        )
        # No variant calls at baseline.
        self.dummy2_var = self.generate_vcf(
            self.target_dir, name="dummy2", platform=TEC_SEQ, time_points=(1,), label=1
        )
        # Variant calls at both time points.
        self.dummy3_var = self.generate_vcf(
            self.target_dir,
            name="dummy3",
            platform=GUARDANT360_74_GENES,
            time_points=(0, 1),
            label=0,
        )
        names = [f"dummy{i}" for i in range(1, 4)]
        self.y = Series([0, 1, 0], index=names, name="Patient ID")
        self.platform = Series(
            [AVENIO_EXPANDED_PANEL, TEC_SEQ, GUARDANT360_74_GENES], index=names
        )


class TestPreprocessingUtils(TestBaseVcfDataGenerator):
    """Test helper functions of preprocessing pipeline."""

    def test_filter_all_zero(self):
        self.generate_vcf(
            self.target_dir,
            name="dummy4",
            platform=IDES_CAPP_SEQ,
            time_points=(0, 1, 2),
        )
        generator = VcfDataGenerator(time_points=["t0", "t1", "t2"])
        X, _ = generator.flow_from_directory(self.target_dir)

        x_dummy4 = X.loc["dummy4"]
        # The indicated KEAP1 variant is only called at t2 (no variant call at
        # t0 + t1).
        x_t2_filtered = filter_not_called(x_dummy4[["t0", "t1"]])

        assert_array_equal(
            x_t2_filtered,
            x_dummy4[["t0", "t1"]].drop(
                index=(
                    "chr19",
                    10489192,
                    "KEAP1",
                    "splice_donor_variant&intron_variant",
                    "c.1708+2G>T",
                )
            ),
        )

        # Similarly for TP53, which is only called at t0 (no variant call at t1
        # + t2).
        x_t0_filtered = filter_not_called(x_dummy4[["t1", "t2"]])
        assert_array_equal(
            x_t0_filtered,
            x_dummy4[["t1", "t2"]].drop(
                index=(
                    "chr17",
                    7675151,
                    "TP53",
                    "missense_variant",
                    "c.461G>T",
                )
            ),
        )


class TestFlowFromDirectory(TestBaseVcfDataGenerator):
    """Test VcfDataGenerator.flow_from_directory function."""

    def test_identity_transformation(self):
        """Test loading when no transformation is applied."""
        generator = VcfDataGenerator()
        X, _ = generator.flow_from_directory(self.target_dir)

        # Generate a "check sum" of variants, per subject.
        X_sum = X.groupby("Patient ID").sum()

        # Dummy 1, only baseline variant calls.
        assert_almost_equal(
            X_sum.loc["dummy1", "t0"], self.dummy1_var[0]["Allele Fraction"].sum()
        )
        assert_almost_equal(X_sum.loc["dummy1", "t1"], 0.0)

        # Dummy 2, only follow-up variant calls.
        assert_almost_equal(X_sum.loc["dummy2", "t0"], 0.0)
        assert_almost_equal(
            X_sum.loc["dummy2", "t1"], self.dummy2_var[1]["Allele Fraction"].sum()
        )

        # Dummy 3, variant calls at both time points.
        assert_almost_equal(
            X_sum.loc["dummy3", "t0"], self.dummy3_var[0]["Allele Fraction"].sum()
        )
        assert_almost_equal(
            X_sum.loc["dummy3", "t1"], self.dummy3_var[1]["Allele Fraction"].sum()
        )

    @skip("Unable to extract platform through `flow_from_directory`.")
    def test_load_clearance(self):
        """Test loading patient with no variant calls at t0 + t1."""
        self.generate_vcf(
            self.target_dir, "dummy4", IDES_CAPP_SEQ, time_points=tuple(), label=1
        )
        generator = VcfDataGenerator(transformations=count_mutations)
        X, y = generator.flow_from_directory(directory=self.target_dir)
        assert_array_equal(X.loc["dummy4"], [0, 0])
        self.assertEqual(int(y.loc["dummy4"]), 1)

    @skip("Unable to extract platform through `flow_from_directory`.")
    def test_single_transformation(self):
        """Load variant calls and apply single transformation."""
        generator = VcfDataGenerator(transformations=count_mutations)
        X, y = generator.flow_from_directory(self.target_dir)

        # Check output label.
        assert_array_equal(y.sort_index().to_numpy(dtype=float), self.y)

        # Dummy1: only baseline with AVENIO.
        expected_1 = [3 / ASSAY_SIZE[AVENIO_EXPANDED_PANEL], 0.0]
        assert_array_equal(X.loc["dummy1"], expected_1)

        # Dummy2: Only follow up with TEC-Seq.
        expected_2 = [0.0, 3 / ASSAY_SIZE[TEC_SEQ]]
        assert_array_equal(X.loc["dummy2"], expected_2)

        # Dummy3: Baseline and follow-up with Guardant360.
        expected_3 = 2 * [3 / ASSAY_SIZE[GUARDANT360_74_GENES]]
        assert_array_equal(X.loc["dummy3"], expected_3)

    @skip("Unable to extract platform through `flow_from_directory`.")
    def test_double_transformation(self):
        """Test applying multiple variant call transformations."""
        generator = VcfDataGenerator(transformations=[count_mutations, total_vaf])
        X, y = generator.flow_from_directory(self.target_dir)

        # Check output label.
        assert_array_equal(y.sort_index().to_numpy(dtype=float), self.y)

        # Dummy1: only baseline with AVENIO.
        dummy1_total_t0 = self.dummy1_var[0]["Allele Fraction"].sum()
        dummy1_total_t1 = 0.0
        expected_1 = [
            # Expected output from `count_mutations`.
            3 / ASSAY_SIZE[AVENIO_EXPANDED_PANEL],
            0.0,
            # Expected output from `total_vaf`.
            dummy1_total_t0,
            dummy1_total_t1,
        ]
        assert_almost_equal(X.loc["dummy1"], expected_1)

        # Dummy3: Baseline and follow-up with Guardant360.
        dummy3_total_t0 = self.dummy3_var[0]["Allele Fraction"].sum()
        dummy3_total_t1 = self.dummy3_var[1]["Allele Fraction"].sum()
        expected_3 = [
            # Expected output from `count_mutations`.
            3 / ASSAY_SIZE[GUARDANT360_74_GENES],
            3 / ASSAY_SIZE[GUARDANT360_74_GENES],
            # Expected output from `total_vaf`.
            dummy3_total_t0,
            dummy3_total_t1,
        ]

        assert_almost_equal(X.loc["dummy3"], expected_3)


class TestFlowFromDataframe(TestBaseVcfDataGenerator):
    """Test VcfDataGenerator.flow_from_dataframe function."""

    def setUp(self):
        """Generate dataframe containing records."""
        super().setUp()

        # Dummy1: only baseline with AVENIO.
        # Dummy2: Only follow up with TEC-Seq.
        # Dummy3: Baseline and follow-up with Guardant360.
        directories = ["0/dummy1", "1/dummy2", "0/dummy3"]
        self.age = [50, 70, 65]
        self.dataframe = DataFrame(
            {
                "directory": directories,
                "class": self.y,
                "age": self.age,
                "platform": self.platform,
            }
        )
        self.dataframe.index.name = "Patient ID"
        self.dataframe.set_index("platform", append=True, inplace=True)
        # Expected values per sample after `count_mutations` transformation.
        self.X_count_expected = DataFrame(
            {
                "count(t0)": [
                    3 / ASSAY_SIZE[AVENIO_EXPANDED_PANEL],
                    0.0,
                    3 / ASSAY_SIZE[GUARDANT360_74_GENES],
                ],
                "count(t1)": [
                    0.0,
                    3 / ASSAY_SIZE[TEC_SEQ],
                    3 / ASSAY_SIZE[GUARDANT360_74_GENES],
                ],
            },
            index=["dummy1", "dummy2", "dummy3"],
        )

    def test_load_clearance(self):
        """Test loading patient with no variant calls at t0 + t1."""
        self.generate_vcf(
            self.target_dir, "dummy4", IDES_CAPP_SEQ, time_points=tuple(), label=1
        )

        dummy4 = ("dummy4", IDES_CAPP_SEQ)
        self.dataframe.loc[dummy4, :] = {"directory": "1/dummy4", "class": 1, "age": 18}
        generator = VcfDataGenerator(transformations=count_mutations)
        X, y = generator.flow_from_dataframe(
            self.dataframe, directory=self.target_dir, keep_columns=False
        )
        self.assertEqual(int(y.loc["dummy4"]), 1)
        assert_array_equal(X.loc["dummy4"], [0, 0])

    def test_passthrough(self):
        """Test that columns are correctly passed through"""
        generator = VcfDataGenerator(transformations=count_mutations)

        X, y = generator.flow_from_dataframe(
            self.dataframe, directory=self.target_dir, keep_columns=True
        )

        # Check output label.
        assert_array_equal(y.sort_index().to_numpy(dtype=float), self.y)

        # Check that "age" column is correctly passed through.
        assert_array_equal(X["age"].sort_index(), self.age)

        X_expected = self.X_count_expected.copy()
        X_expected["age"] = self.age

        self.assertEqual(X.shape, X_expected.shape)
        assert_array_equal(X.loc["dummy1"], X_expected.loc["dummy1"])
        assert_array_equal(X.loc["dummy2"], X_expected.loc["dummy2"])
        assert_array_equal(X.loc["dummy3"], X_expected.loc["dummy3"])

    def test_no_label(self):
        """Test loading without labels `y` (i.e., `class_mode=None`)."""
        # No class label when using `keep_columns=True`.
        X_annotated = VcfDataGenerator(
            transformations=count_mutations
        ).flow_from_dataframe(
            self.dataframe,
            directory=self.target_dir,
            keep_columns=True,
            class_mode=None,
        )
        self.assertNotIn("class", X_annotated.columns)

        # Compute number of mutations and compare with expected output.
        X_mutation = VcfDataGenerator(
            transformations=count_mutations
        ).flow_from_dataframe(
            self.dataframe,
            directory=self.target_dir,
            keep_columns=False,
            class_mode=None,
        )
        self.assertNotIn("class", X_mutation.columns)

        self.assertEqual(X_mutation.shape, self.X_count_expected.shape)
        assert_array_equal(
            X_mutation.loc["dummy1"], self.X_count_expected.loc["dummy1"]
        )
        assert_array_equal(
            X_mutation.loc["dummy2"], self.X_count_expected.loc["dummy2"]
        )
        assert_array_equal(
            X_mutation.loc["dummy3"], self.X_count_expected.loc["dummy3"]
        )

    @skip("No platform support for `flow_from_directory`.")
    def test_flow_from_dataframe_directory(self):
        """Test that flow_from_{directory,dataframe} generates the same data."""
        generator = VcfDataGenerator(transformations=[count_mutations, total_vaf])
        X_dir = generator.flow_from_directory(self.target_dir, class_mode=None)
        X_df = generator.flow_from_dataframe(
            self.dataframe,
            directory=self.target_dir,
            keep_columns=False,
            class_mode=None,
        )
        assert_array_equal(X_dir.sort_index(), X_df.sort_index())


class TestMolecularResponse(TestBaseVcfDataGenerator):
    def setUp(self):
        """Add extra non-shedder."""
        super().setUp()
        # No variant calls at either time points (non-shedder).
        self.dummy4_var = self.generate_vcf(
            self.target_dir,
            name="dummy4",
            platform=IDES_CAPP_SEQ,
            time_points=tuple(),
            label=1,
        )
        self.y = Series(
            [0, 1, 0, 1], index=[f"dummy{i}" for i in range(1, 5)], name="Patient ID"
        )
        directories = [f"{label}/{idx}" for idx, label in self.y.items()]

        # Build a dataframe with a (`Patient ID`, `platform`) multi-index.
        self.dataframe = DataFrame(
            {"directory": directories, "class": self.y, "platform": IDES_CAPP_SEQ}
        )
        self.dataframe.index.name = "Patient ID"
        self.dataframe.set_index("platform", append=True, inplace=True)

        dg = VcfDataGenerator(transformations=molecular_response_layer_old)
        self.X, _ = dg.flow_from_dataframe(self.dataframe, directory=self.target_dir)

    def test_molecular_response(self):
        """Test molecular response based on most abundant baseline variant."""
        # In the fabricated data, the ABL1 variant is the largest variant both
        # at t0 and t1.
        name = "dummy5"
        variants_at_t0, variants_at_t1, _ = fabricate_variant_calls(name)

        # Generate test data where the largest mutations are:
        # - ABL1 at t0
        # - TERT at t1.
        # by swapping ABL1 <--> TERT Allele Fraction (column 6).

        # Order not guaranteerd, so sort first.
        variants_at_t1.sort_values("Gene", inplace=True)
        variants_at_t1.iloc[0, 6], variants_at_t1.iloc[2, 6] = (
            variants_at_t1.iloc[2, 6],
            variants_at_t1.iloc[0, 6],
        )

        # Store data to disk.
        variants = concat([variants_at_t0, variants_at_t1])
        data = CtdnaDataset(name="test", platform="test", reference_genome="?")
        y = Series([0], index=[name])
        data.to_vcf(variants, y, target_dir=self.target_dir)
        record = DataFrame(
            {"directory": f"0/{name}", "class": 0},
            index=[(name, "test")],
        )
        self.dataframe = self.dataframe.append(record)

        # Read VCF from disk.
        dg = VcfDataGenerator(transformations=molecular_response_layer)
        X, _ = dg.flow_from_dataframe(self.dataframe, directory=self.target_dir)
        assert_almost_equal(X.loc[name, "vaf_ratio"], 0.02 / 0.0339)

    def test_hvaf_50(self):
        """Check that 50 per cent reductions are correctly derived."""
        actual = [1, 0, 0, 0]
        assert_array_equal(actual, self.X[r"hvaf>50%red"].sort_index())

    def test_clearance(self):
        """Test t1 clearance indicator is correctly determined."""
        actual = [1, 0, 0, 1]
        assert_array_equal(actual, self.X["clearance_t1"].sort_index())

    def test_non_shedder(self):
        """Test molecular response transform correctly identifies non-shedder."""
        actual = [0, 0, 0, 1]
        assert_array_equal(actual, self.X["clearance_t0+t1"].sort_index())


class TestGeneRatio(TestBaseVcfDataGenerator):
    """Test gene ratio computation based on largest baseline variant."""

    def setUp(self):
        super().setUp()
        name = "dummy5"
        variants_t0_0, variants_t1_0, _ = fabricate_variant_calls(name)
        variants_t0_0["Variant Description"] = ""
        variants_t1_0["Variant Description"] = ""

        def duplicate(variants_in):
            variants = variants_in.copy()
            variants["Allele Fraction"] += 0.1
            variants["Genomic Position"] = variants["Genomic Position"] + "0"
            return variants

        # Double the variants, at slight different positions and allele freq.
        variants_t0_1 = duplicate(variants_t0_0)
        variants_t1_1 = duplicate(variants_t1_0)

        # Swap larger and smaller.
        (
            variants_t1_0.loc[:, "Allele Fraction"],
            variants_t1_1.loc[:, "Allele Fraction"],
        ) = (
            variants_t1_1["Allele Fraction"].copy(),
            variants_t1_0["Allele Fraction"].copy(),
        )

        # Store data to disk.
        self.variants = concat(
            [variants_t0_0, variants_t0_1, variants_t1_0, variants_t1_1]
        )
        data = CtdnaDataset(
            name="test", platform=AVENIO_SURVEILLANCE_PANEL, reference_genome="?"
        )
        y = Series([0], index=[name])
        data.to_vcf(self.variants, y, target_dir=self.target_dir)
        self.dataframe = DataFrame(
            {
                "directory": f"0/{name}",
                "class": 0,
                "platform": AVENIO_SURVEILLANCE_PANEL,
            },
            index=[name],
        )
        self.dataframe.index.name = "Patient ID"
        self.dataframe.set_index("platform", append=True, inplace=True)

    def test_gene_ratio_baseline(self):
        """Test ratio computation when there is more than one mutation per gene."""
        dg = VcfDataGenerator(
            transformations=lambda x: gene_ratio_layer(x, vocab={"TP53", "TERT", "MET"})
        )
        self.X, _ = dg.flow_from_dataframe(self.dataframe, directory=self.target_dir)
        assert_almost_equal(self.X.loc["dummy5", "TP53"], 0 / 0.1114)
        assert_almost_equal(self.X.loc["dummy5", "TERT"], 0.02 / 0.1150)
        assert_almost_equal(self.X.loc["dummy5", "MET"], inf)

        # Out-of-vocabulary mutation: ABL1, chr9:1308628340.
        assert_almost_equal(self.X.loc["dummy5", "oov"], 0.0915 / 0.1339)


class TestChromosomeLayer(TestBaseVcfDataGenerator):
    def setUp(self):
        """Add extra non-shedder."""
        super().setUp()
        # No variant calls at either time points (non-shedder).
        self.dummy4_var = self.generate_vcf(
            self.target_dir,
            name="dummy4",
            platform=IDES_CAPP_SEQ,
            time_points=tuple(),
            label=1,
        )
        self.y["dummy4"] = 1
        self.platform["dummy4"] = IDES_CAPP_SEQ

        directories = [f"{label}/{idx}" for idx, label in self.y.items()]
        dataframe = DataFrame(
            {"directory": directories, "class": self.y, "platform": self.platform}
        )
        dataframe.index.name = "Patient ID"
        dataframe.set_index("platform", append=True, inplace=True)

        dg = VcfDataGenerator(transformations=chromosome_layer)
        self.X, _ = dg.flow_from_dataframe(dataframe, directory=self.target_dir)

    def test_down(self):
        """Check that increments are correctly identified."""
        x = self.X.loc["dummy1"]
        # No follow-up variant calls => all variants have 50 % reduction.
        down_chromosomes = ["chr5↓", "chr9↓", "chr17↓"]
        self.assertTrue((x[down_chromosomes] == 1).all())

        # The remainder must be nan or zero.
        others = x.index.difference(down_chromosomes)
        nan_or_zero = x[others].isna() | (x[others] == 0)
        self.assertTrue(nan_or_zero.all())

    def test_up(self):
        """Check that increments are correctly identified."""
        x = self.X.loc["dummy2"]
        # No follow-up variant calls => all variants have 50 % reduction.
        up_chromosomes = ["chr5↑", "chr7↑", "chr9↑"]
        self.assertTrue((x[up_chromosomes] == 1).all())

        # The remainder must be nan or zero.
        others = x.index.difference(up_chromosomes)
        nan_or_zero = x[others].isna() | (x[others] == 0)
        self.assertTrue(nan_or_zero.all())

    def test_both(self):
        """ """
        x = self.X.loc["dummy3"]
        # ABL1 -> chr9; MET -> chr7.
        up_chromosomes = ["chr7↑", "chr9↑"]
        self.assertTrue((x[up_chromosomes] == 1).all())

        # TP53 -> chr17
        down_chromosomes = ["chr17↓"]
        self.assertTrue((x[down_chromosomes] == 1).all())

        # The remainder must be nan or zero.
        others = x.index.difference(up_chromosomes + down_chromosomes)
        nan_or_zero = x[others].isna() | (x[others] == 0)
        self.assertTrue(nan_or_zero.all())

    def test_non_shedder(self):
        """No changes for non-shedder => all values must be nan or zero."""
        x = self.X.loc["dummy4"]
        nan_or_zero = x.isna() | (x == 0)
        self.assertTrue(nan_or_zero.all())

    def test_support_mask(self):
        """Check that all variables not supported by panel are nan."""
        chr_up = [f"{chrom}↑" for chrom in TARGET_CHROMS[TEC_SEQ]]
        chr_down = [f"{chrom}↓" for chrom in TARGET_CHROMS[TEC_SEQ]]
        not_supported = self.X.columns.difference(chr_up + chr_down)
        self.assertTrue(self.X.loc["dummy2", not_supported].isna().all())


class TestReproduceOldPipeline(TestCase):
    """Try to reproduce artifacts from previous pipeline."""

    tmb_features = ["count(t0)", "count(t1)"]
    mol_resp_features = [r"hvaf>50%red", "clearance_t1", "clearance_t0+t1"]

    def setUp(self):
        """Build artifacts for new pipeline."""
        artifact_dir = Path(mkdtemp())
        step1_build_vcf_artifacts(target_dir=artifact_dir / "1-vcf")
        step2_build_train_dev_test_split(
            source_dir=artifact_dir / "1-vcf", target_dir=artifact_dir / "2-split"
        )
        df = read_csv(
            artifact_dir / "2-split" / "nsclc-immunotherapy" / "all.csv",
            index_col=["Patient ID", "platform"],
        )

        dg = VcfDataGenerator(
            transformations=[
                count_mutations,
                molecular_response_layer_old,
                chromosome_layer,
            ]
        )
        self.X, _ = dg.flow_from_dataframe(df, keep_columns=False)
        self.patient_ids_to_test = [
            "ANAG211",
            "NABE335",
            "THOM355",
            "THOM672",
            "WEBE1057",
        ]

    def assert_non_zero(self, x, non_zero):
        """Test that only the element in `non_zero` are 1."""
        self.assertTrue((x[non_zero] == 1).all())

        others = x.index.difference(
            self.tmb_features + self.mol_resp_features + non_zero
        )
        self.assertTrue(((x[others] == 0) | x[others].isna()).all())

    def test_mutation_count(self):
        """Compare the coverage-corrected mutation count with previous pipeline."""
        # Values from the previous pipeline.
        tmb = [
            [12.34567901, 0],
            [16.90140845, 16.90140845],
            [20, 20],
            [33.33333333, 6.666666667],
            [10.41666667, 10.41666667],
        ]

        assert_almost_equal(
            self.X.loc[self.patient_ids_to_test, self.tmb_features],
            tmb,
        )

    def test_molecular_response_layer(self):
        """Compare the molecular response markers with previous pipeline."""
        # Values from the previous pipeline.
        mol_resp = [
            [1, 1, 0],
            [0, 0, 0],
            [0, 0, 0],
            [1, 0, 0],
            [0, 0, 0],
        ]

        assert_almost_equal(
            self.X.loc[
                self.patient_ids_to_test,
                self.mol_resp_features,
            ],
            mol_resp,
        )

    def test_chromosome_layer(self):
        """Compare chromosome layer features with previous pipeline."""
        # The values marked as non_zero were obtained from the artifacts of the
        # previous pipeline.
        self.assert_non_zero(self.X.loc["ANAG211"], non_zero=["chr17↓"])
        self.assert_non_zero(self.X.loc["NABE335"], non_zero=["chr11↓"])
        self.assert_non_zero(
            self.X.loc["THOM355"], non_zero=["chr12↓", "chr13↓", "chr17↑", "chr20↑"]
        )
        self.assert_non_zero(
            self.X.loc["THOM672"],
            non_zero=["chr13↓", "chr17↓", "chr17↑", "chr19↓", "chr3↓", "chrX↓"],
        )
        self.assert_non_zero(self.X.loc["WEBE1057"], non_zero=["chr17↑"])


class TestPipelineArtifacts(TestCase):
    """Check consistency of produced artifacts."""

    def setUp(self):
        """Build artifacts for new pipeline."""
        self.artifact_dir = Path(mkdtemp())
        csv_dir = self.artifact_dir / "2-split"

        step1_build_vcf_artifacts(target_dir=self.artifact_dir / "1-vcf")
        step2_build_train_dev_test_split(
            source_dir=self.artifact_dir / "1-vcf", target_dir=csv_dir
        )
        self.df_all = read_csv(
            csv_dir / "nsclc-immunotherapy" / "all.csv",
            index_col=["Patient ID", "platform"],
        )

    def tearDown(self):
        """Remove temporary artifact dir."""
        rmtree(self.artifact_dir)

    def test_time_weeks(self):
        """Test that sample timing is correctly unpacked."""
        # Check for specific case: ZOU16358.
        X, y = VcfDataGenerator(time_points=[]).flow_from_dataframe(
            self.df_all,
            drop_time=False,
            keep_columns=False,
        )
        self.assertEqual(y.loc["ZOU16358"], "pfs<6months")
        x_i = X.loc["ZOU16358"].dropna(axis="columns", how="any")

        # Check baseline timing.
        t0 = x_i["t0"]
        time_weeks_t0 = t0.columns.get_level_values(0)
        assert_array_equal(time_weeks_t0, 0.0)
        assert_array_equal(t0[t0 != 0.0].count(), 22)

        # First follow up.
        t1 = x_i["t1"]
        time_weeks_t1 = t1.columns.get_level_values(0)
        assert_array_equal(time_weeks_t1, 3.0)
        assert_array_equal(t1[t1 != 0.0].count(), 24)

        # First follow up.
        t2 = x_i["t2"]
        time_weeks_t2 = t2.columns.get_level_values(0)
        assert_array_equal(time_weeks_t2, 6.0)
        assert_array_equal(t2[t2 != 0.0].count(), 17)

        # First follow up.
        t3 = x_i["t3"]
        time_weeks_t3 = t3.columns.get_level_values(0)
        assert_array_equal(time_weeks_t3, 9.0)
        assert_array_equal(t3[t3 != 0.0].count(), 14)

    def test_timing_complete(self):
        """Test that we have sample timing for all measurements."""
        X_train_raw, _ = VcfDataGenerator().flow_from_dataframe(
            self.df_all, keep_columns=False, drop_time=False
        )

        # Make sure that each sample contains timing info.
        self.assertFalse(
            X_train_raw.columns.get_level_values("time_weeks").isna().any()
        )
        # Check specific instance that gave problems.
        x_nabe338 = X_train_raw.loc["NABE338"].dropna(axis="columns", how="any")
        self.assertEqual(x_nabe338.columns[0], ("t0", 0.0))
        self.assertEqual(x_nabe338.columns[1], ("t1", 4.0))

    def test_vcf_number_of_variants(self):
        """Test that VCF artifacts contain correct number of variants."""
        anag111 = ("ANAG111", TEC_SEQ)
        vcf_dir = Path(self.df_all.loc[anag111, "directory"])
        vcf_files = tuple(vcf_dir.glob("*.vcf"))
        self.assertEqual(len(vcf_files), 8)

        self.assertEqual(line_count(vcf_dir / "t0.vcf"), 1)
        for time_point in range(1, 8):
            filename = vcf_dir / f"t{time_point}.vcf"
            with self.subTest(
                msg=f"Testing number of variants at time point {time_point}."
            ):
                self.assertEqual(line_count(filename), 0)

    def test_chemotherapy(self):
        """Test that chemotherapy artifacts are generated."""
        chemo_zou = read_csv(
            self.artifact_dir / "1-vcf" / "nsclc-chemo" / "zou" / "pfs.csv",
            index_col=["Patient ID", "platform"],
        )

        # Test specific instance.
        # 1) Number of variant calls.
        ZOU50080 = ("ZOU50080", AVENIO_SURVEILLANCE_PANEL)
        vcf_dir = Path(chemo_zou.loc[ZOU50080, "directory"])
        self.assertEqual(line_count(vcf_dir / "t0.vcf"), 3)
        self.assertEqual(line_count(vcf_dir / "t1.vcf"), 5)

        # 2) Clinical characeristics that enter the model.
        self.assertEqual(chemo_zou.loc[ZOU50080, "histology_squamous"], 1)
        self.assertEqual(chemo_zou.loc[ZOU50080, "therapyline>1"], 1)
        self.assertEqual(chemo_zou.loc[ZOU50080, "smoker"], 1)

    def test_vcf_info_field_types(self):
        """Test that the info types in the header match the fields."""
        anag111 = ("ANAG111", TEC_SEQ)
        vcf_dir = Path(self.df_all.loc[anag111, "directory"])
        reader = vcfpy.Reader.from_path(vcf_dir / "t0.vcf")

        record = next(reader)
        af_type = reader.header.get_info_field_info("AF")
        mm_type = reader.header.get_info_field_info("MM")
        dp_type = reader.header.get_info_field_info("DP")
        ad_type = reader.header.get_info_field_info("AD")

        def assert_field_type(meta_data, field):
            """Check type of INFO field with definition in header."""
            if meta_data.type == "Float":
                variable_type = float
            elif meta_data.type == "Integer":
                variable_type = int

            if meta_data.number in ("A", "R"):
                self.assertTrue(isinstance(field, Iterable))
                correct_type = (
                    item is None or isinstance(item, variable_type) for item in field
                )
                self.assertTrue(all(correct_type))
            elif meta_data.number == 1:
                self.assertTrue(field is None or isinstance(field, variable_type))

        assert_field_type(af_type, record.INFO["AF"])
        assert_field_type(mm_type, record.INFO["MM"])
        assert_field_type(ad_type, record.INFO["AD"])
        assert_field_type(dp_type, record.INFO["DP"])

    def test_variant_consequences(self):
        """Test for out-of-vocabulary variant consequences."""
        X, _ = VcfDataGenerator().flow_from_dataframe(self.df_all, keep_columns=False)
        variant_types = X.reset_index()["variant_type"].unique()
        unique_variants = unpack_variants(variant_types)
        # We don't know what to do with 'promoter_region_variant', so we will
        # just allow it.
        self.assertFalse(unique_variants - VARIANT_VOCABULARY)
        self.assertTrue(unique_variants.issubset(VARIANT_VOCABULARY))

    def test_clearance_count_ratio(self):
        """Test consistency between variant count ratio and clearance markers."""
        X, _ = VcfDataGenerator(
            transformations=[molecular_response_layer_old, count_ratio]
        ).flow_from_dataframe(self.df_all, keep_columns=False)

        # No mutations at t1.
        X_clear_t1 = X[X["#mutations"] == 0]
        self.assertTrue(all(X_clear_t1[["clearance_t1", "clearance_t0+t1"]] == [1, 0]))

        # No mutations at t0 + t1.
        X_non_shed = X[X["#mutations"] == 1]
        assert all(X_non_shed[["clearance_t1", "clearance_t0+t1"]] == [1, 1])

        # Clearance at t0.
        X_clear_t0 = X[X["#mutations"] == 2]
        assert all(X_clear_t0[["clearance_t1", "clearance_t0+t1"]] == [0, 0])

    def test_artifact_annotation(self):
        """Test annotation filter applied to VCF artifacts."""
        thom279 = ("THOM279", GUARDANT360_74_GENES)
        vcf_dir = self.df_all.loc[thom279, "directory"]
        source_vcf = Path(vcf_dir) / "t0.vcf"

        with TemporaryDirectory() as temp_dir:
            destination_vcf = Path(temp_dir) / "t0.vcf"
            apply_pathogenic_variant_filter(source_vcf, destination_vcf)
            with open(destination_vcf, "r") as fo:
                lines = fo.read().splitlines()

        variant_lines = list(filter(lambda x: not x.startswith("#"), lines))
        # Check that the files have been correctly annotated.
        self.assertEqual(len(list(filter(lambda x: "PASS" in x, variant_lines))), 1)
        self.assertEqual(
            len(list(filter(lambda x: "ILL_POSED" in x, variant_lines))), 1
        )
        self.assertEqual(len(list(filter(lambda x: "NON_PATH" in x, variant_lines))), 3)

        # Check that there are two filter lines.
        filter_lines = filter(lambda x: x.startswith("##FILTER"), lines)
        self.assertEqual(len(list(filter_lines)), 2)
