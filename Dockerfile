# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.10

# # Keeps Python from generating .pyc files in the container
# ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install pip requirements
RUN pip install --upgrade pip

# Problem: Shap and pomegranate have conflicting NumPy dependencies.
#
# Solution:
# 1) Install shap with corresponding NumPy version.
RUN pip install shap
# 2) Compile pomegranate from source for that specific NumPy version.
# RUN pip install pomegranate
RUN cd /opt/ && git clone https://github.com/jmschrei/pomegranate.git
WORKDIR /opt/pomegranate/
RUN pip install -r dev-requirements.txt
RUN python setup.py build
RUN python setup.py build_ext --inplace
RUN python setup.py install
# 3) Pip install remaining packages.
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . /app

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-python-configure-containers
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["python3", "-m", "unittest", "discover", "test"]
