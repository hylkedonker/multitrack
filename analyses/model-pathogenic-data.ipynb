{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Comparison pathogenic versus non-pathogenic mutants\n",
    "We determine if the model improves by keeping only the pathogenic variants."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "\n",
    "import optuna\n",
    "from sklearn.metrics import roc_auc_score"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Add parent directory to path.\n",
    "import os\n",
    "import sys\n",
    "sys.path.insert(1, os.path.join(sys.path[0], '..'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ctdna.model.architectures import build_train_ctdna_model\n",
    "from ctdna.model.utils import compute_sample_weights\n",
    "from ctdna.dataset import load_splits\n",
    "from ctdna.plot.views import confidence_interval_label\n",
    "from ctdna.statistics import paired_permutation_test\n",
    "from ctdna.vcf.preprocessing import (\n",
    "    VcfDataGenerator, \n",
    "    gene_ratio_layer,\n",
    "    select_next_follow_up,\n",
    "    filter_not_called,\n",
    ")\n",
    "from ctdna.utils import stratified_score"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "optuna.logging.set_verbosity(optuna.logging.WARNING)\n",
    "logging.basicConfig(level=logging.INFO)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic statistics of pathogen filtered variants"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load unfiltered variants.\n",
    "df_all = load_splits(all=True)\n",
    "df_train, df_dev, df_test = load_splits()\n",
    "\n",
    "# Load only variants classified by ClinVar as (likely) pathogenic.\n",
    "df_path_all = load_splits(all=True, step=4)\n",
    "df_path_train, df_path_dev, df_path_test = load_splits(step=4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_all_pathg, _ = VcfDataGenerator().flow_from_dataframe(df_path_all, keep_columns=False)\n",
    "X_all, _ = VcfDataGenerator().flow_from_dataframe(df_all, keep_columns=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Out of 2445 variants, 519 were marked as (likely) pathogenic (21.2 %)\n"
     ]
    }
   ],
   "source": [
    "n_variants = filter_not_called(X_all).shape[0]\n",
    "n_variants_path = filter_not_called(X_all_pathg).shape[0]\n",
    "percent_path = (n_variants_path/n_variants) * 100\n",
    "print(f'Out of {n_variants} variants, {n_variants_path} were marked as (likely) pathogenic ({percent_path:.1f} %)')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model performance comparison\n",
    "Let's now generate features from these datasets, and train the model with the seven gene multitrack architecture (no clinical features) to compare performance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "def generate_sets(df_train, df_dev, df_test):\n",
    "    dg = VcfDataGenerator(\n",
    "        transformations=gene_ratio_layer,\n",
    "        filters=select_next_follow_up,\n",
    "    )\n",
    "    X_train, y_train = dg.flow_from_dataframe(df_train, keep_columns=True)\n",
    "    weights = compute_sample_weights(X_train)\n",
    "\n",
    "    X_dev, y_dev = dg.flow_from_dataframe(df_dev, keep_columns=True)\n",
    "    X_test, y_test = dg.flow_from_dataframe(df_test, keep_columns=True)\n",
    "\n",
    "    to_binary = {'pfs≥6months': 1, 'pfs<6months': 0}\n",
    "    y_train = y_train.replace(to_binary)\n",
    "    y_dev = y_dev.replace(to_binary)\n",
    "    y_test = y_test.replace(to_binary)\n",
    "        \n",
    "    return (X_train, y_train), (X_dev, y_dev), (X_test, y_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "(X_train, y_train), (X_dev, y_dev), (X_test, y_test) = generate_sets(df_train, df_dev, df_test)\n",
    "(X_path_train, y_path_train), (X_path_dev, y_path_dev), (X_path_test, y_path_test) = generate_sets(df_path_train, df_path_dev, df_path_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "def predictiveness(model, X_train, y_train, X_dev, y_dev, X_test, y_test) -> float:\n",
    "    \"\"\"Compute ROC AUC on data slices and return classification threshold.\"\"\"\n",
    "    features = list(model.distributions.keys())\n",
    "\n",
    "    y_train_prob = model.predict_proba(X_train[features]).iloc[:,1]\n",
    "    train_score = stratified_score(y_train, y_train_prob, metric=roc_auc_score)\n",
    "    logging.info('Score on train set')\n",
    "    logging.info(train_score)\n",
    "\n",
    "    y_dev_prob = model.predict_proba(X_dev[features]).iloc[:,1]\n",
    "    dev_score = stratified_score(y_dev, y_dev_prob, metric=roc_auc_score)\n",
    "    logging.info('Score on dev set')\n",
    "    logging.info(dev_score)\n",
    "\n",
    "    y_test_prob = model.predict_proba(X_test[features]).iloc[:,1]\n",
    "    y_test_prob_multitrack = y_test_prob.copy()\n",
    "    test_score = stratified_score(y_test, y_test_prob_multitrack, metric=roc_auc_score)\n",
    "    logging.info('Score on test set')\n",
    "    logging.info(test_score)\n",
    "    \n",
    "    print('train', confidence_interval_label(train_score.loc['overall']))\n",
    "    print('dev', confidence_interval_label(dev_score.loc['overall']))\n",
    "    print('test', confidence_interval_label(test_score.loc['overall']))\n",
    "    return y_test_prob"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Model without pathogenicity filter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Tuning pseudo counts...\n",
      "{'pseudo_count': 1.2664296373528532}\n",
      "Re-training tuned model... [DONE]\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "INFO:root:Score on train set\n",
      "INFO:root:                            point     lower     upper\n",
      "overall                  0.818343  0.760727  0.867688\n",
      "Anagnostou et al. ('19)   0.71875  0.266667       1.0\n",
      "Nabet et al. ('20)       0.775974  0.641056  0.893668\n",
      "Thompson et al. ('21)      0.7849  0.633911  0.905963\n",
      "Weber et al. ('21)         0.7959  0.705539  0.879011\n",
      "Zou et al. ('21)         0.906433  0.729132       1.0\n",
      "INFO:root:Score on dev set\n",
      "INFO:root:                            point     lower     upper\n",
      "overall                  0.750379  0.629163  0.857918\n",
      "Anagnostou et al. ('19)       1.0       1.0       1.0\n",
      "Nabet et al. ('20)       0.839286      0.58       1.0\n",
      "Thompson et al. ('21)    0.671875   0.34375  0.938304\n",
      "Weber et al. ('21)       0.702652   0.49277    0.8789\n",
      "Zou et al. ('21)         0.611111      0.25      0.95\n",
      "INFO:root:Score on test set\n",
      "INFO:root:                            point     lower     upper\n",
      "overall                  0.773425  0.666109  0.869725\n",
      "Anagnostou et al. ('19)      0.75       0.5       1.0\n",
      "Nabet et al. ('20)        0.77551       0.5   0.96875\n",
      "Thompson et al. ('21)    0.844444  0.582292       1.0\n",
      "Weber et al. ('21)       0.698347  0.487378   0.87652\n",
      "Zou et al. ('21)         0.916667    0.6875       1.0\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "train 0.82$^{+0.05}_{-0.06}$\n",
      "dev 0.75$^{+0.11}_{-0.12}$\n",
      "test 0.77$^{+0.10}_{-0.11}$\n"
     ]
    }
   ],
   "source": [
    "model = build_train_ctdna_model(X_train, y_train, X_dev, y_dev)\n",
    "y_test_prob = predictiveness(model, X_train, y_train, X_dev, y_dev, X_test, y_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Model with only (likely) pathogenic variants."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Tuning pseudo counts...\n",
      "{'pseudo_count': 0.31298320746735286}\n",
      "Re-training tuned model... [DONE]\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "INFO:root:Score on train set\n",
      "INFO:root:                            point     lower     upper\n",
      "overall                  0.769436  0.712306   0.82573\n",
      "Anagnostou et al. ('19)   0.96875    0.8125       1.0\n",
      "Nabet et al. ('20)       0.727273  0.591616  0.844919\n",
      "Thompson et al. ('21)     0.75641   0.60813  0.880009\n",
      "Weber et al. ('21)       0.710339  0.612589  0.797535\n",
      "Zou et al. ('21)         0.883041  0.746793  0.986673\n",
      "INFO:root:Score on dev set\n",
      "INFO:root:                            point     lower     upper\n",
      "overall                  0.778788   0.66129    0.8773\n",
      "Anagnostou et al. ('19)       1.0       1.0       1.0\n",
      "Nabet et al. ('20)         0.6875  0.409935  0.925992\n",
      "Thompson et al. ('21)      0.8125    0.5625       1.0\n",
      "Weber et al. ('21)       0.729167  0.520619  0.914073\n",
      "Zou et al. ('21)         0.777778  0.464286       1.0\n",
      "INFO:root:Score on test set\n",
      "INFO:root:                            point     lower     upper\n",
      "overall                  0.640169  0.509475  0.754097\n",
      "Anagnostou et al. ('19)       0.5       0.5       0.5\n",
      "Nabet et al. ('20)       0.714286      0.45  0.927083\n",
      "Thompson et al. ('21)    0.733333  0.399444       1.0\n",
      "Weber et al. ('21)       0.644628  0.439996  0.813542\n",
      "Zou et al. ('21)         0.666667      0.25       1.0\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "train 0.77$^{+0.06}_{-0.06}$\n",
      "dev 0.78$^{+0.10}_{-0.12}$\n",
      "test 0.64$^{+0.11}_{-0.13}$\n"
     ]
    }
   ],
   "source": [
    "model_path = build_train_ctdna_model(X_path_train, y_path_train, X_path_dev, y_path_dev)\n",
    "y_path_test_prob = predictiveness(model_path, X_path_train, y_path_train, X_path_dev, y_path_dev, X_path_test, y_path_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Is the difference statistically significant?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Statistical significance p=0.029\n"
     ]
    }
   ],
   "source": [
    "_, p = paired_permutation_test(\n",
    "    y_test,\n",
    "    y_test_prob,\n",
    "    y_path_test_prob,\n",
    "    metric=roc_auc_score,\n",
    ")\n",
    "print(f'Statistical significance p={p}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Data not shown here: If you re-prune the model with the top-20 recurrently mutated genes based on the pathogenic variants, you still get a bad model."
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "e7370f93d1d0cde622a1f8e1c04877d8463912d04d973331ad4851f04de6915a"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
