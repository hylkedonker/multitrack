{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Beyond non-small cell lung cancer\n",
    "In this notebook we analyse the extent our model is applicable to:\n",
    "- Different treatment regimens: In this case, chemotherapy instead of non-small cell lung cancer (Zou's chemotherapy control arm)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "from typing import Literal\n",
    "\n",
    "from numpy import array, mean\n",
    "import optuna\n",
    "from pandas import concat, read_csv\n",
    "from sklearn.metrics import roc_auc_score\n",
    "from sklearn.utils import shuffle"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Add parent directory to path.\n",
    "import os\n",
    "import sys\n",
    "\n",
    "sys.path.insert(1, os.path.join(sys.path[0], \"..\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ctdna.dataset import load_splits, get_latest_artifact\n",
    "from ctdna.model.architectures import (\n",
    "    build_train_clinical_ctdna_model,\n",
    "    build_train_ctdna_model,\n",
    ")\n",
    "from ctdna.model.utils import compute_sample_weights\n",
    "from ctdna.statistics import bootstrap_score, unpaired_permutation_test\n",
    "from ctdna.utils import stratified_score\n",
    "from ctdna.vcf.preprocessing import (\n",
    "    VcfDataGenerator,\n",
    "    gene_ratio_layer,\n",
    "    select_next_follow_up,\n",
    ")\n",
    "from ctdna.plot.views import confidence_interval_label"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "optuna.logging.set_verbosity(optuna.logging.WARNING)\n",
    "logging.basicConfig(level=logging.INFO)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Load and preprocess the mutation data. Both for the immunotherapy datasets, and the chemotherapy cohort."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load immunotherapy splits.\n",
    "df_train, df_dev, df_test = load_splits()\n",
    "\n",
    "# Load chemotherapy dataset.\n",
    "artifact_path = get_latest_artifact()\n",
    "splits_path = artifact_path / \"1-vcf\" / \"nsclc-chemo\"\n",
    "df_chemo = read_csv(splits_path / \"zou/pfs.csv\", index_col=[\"Patient ID\", \"platform\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "dg = VcfDataGenerator(\n",
    "    transformations=gene_ratio_layer,\n",
    "    filters=select_next_follow_up,\n",
    ")\n",
    "X_train, y_train = dg.flow_from_dataframe(df_train, keep_columns=True)\n",
    "weights = compute_sample_weights(X_train)\n",
    "\n",
    "X_dev, y_dev = dg.flow_from_dataframe(df_dev, keep_columns=True)\n",
    "\n",
    "# Test sets of immuno and chemo therapy.\n",
    "X_test_immuno, y_test_immuno = dg.flow_from_dataframe(df_test, keep_columns=True)\n",
    "X_test_chemo, y_test_chemo = dg.flow_from_dataframe(df_chemo, keep_columns=True)\n",
    "\n",
    "to_binary = {'pfs≥6months': 1, 'pfs<6months': 0}\n",
    "y_train = y_train.replace(to_binary)\n",
    "y_dev = y_dev.replace(to_binary)\n",
    "y_test_immuno = y_test_immuno.replace(to_binary)\n",
    "y_test_chemo = y_test_chemo.replace(to_binary)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Number of chemotherapy treated patients:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "48"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "len(y_test_chemo)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Models"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We try both architectures, with and without clinical characteristics.\n",
    "## CtDNA only\n",
    "Train our immunotherapy model as usual."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Tuning pseudo counts...\n",
      "{'pseudo_count': 1.2664296373528532}\n",
      "Re-training tuned model... [DONE]\n"
     ]
    }
   ],
   "source": [
    "model_ctdna = build_train_ctdna_model(X_train, y_train, X_dev, y_dev)\n",
    "features = list(model_ctdna.distributions.keys())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make actual comparison between immunotherapy and chemotherapy predictions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_test_prob_immuno = model_ctdna.predict_proba(X_test_immuno[features]).iloc[:,1]\n",
    "immuno_score = stratified_score(y_test_immuno, y_test_prob_immuno, metric=roc_auc_score)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_test_prob_chemo = model_ctdna.predict_proba(X_test_chemo[features]).iloc[:,1]\n",
    "chemo_score = bootstrap_score(\n",
    "    y_true=y_test_chemo,\n",
    "    y_pred=y_test_prob_chemo,\n",
    "    metric=roc_auc_score,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'0.58^{+0.18}_{-0.18}'"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "confidence_interval_label(chemo_score).replace('$', '')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "def permutation_test(\n",
    "    y_test_1,\n",
    "    y_pred_1,\n",
    "    y_test_2,\n",
    "    y_pred_2,\n",
    "    metric,\n",
    "    alternative: Literal[\"less\", \"greater\", \"two-sided\"] = \"two-sided\",\n",
    "    n_iterations=1000,\n",
    "    random_state=1234,\n",
    "):\n",
    "    \"\"\"Test if first pair (y_{test, pred}_1) is significantly smaller than the second pair.\n",
    "\n",
    "    H0: metric is not different.\n",
    "    \"\"\"\n",
    "    score1 = metric(y_test_1, y_pred_1)\n",
    "    score2 = metric(y_test_2, y_pred_2)\n",
    "    observed_difference = score1 - score2\n",
    "    \n",
    "    pair_1 = concat([y_test_1, y_pred_1], axis=1)\n",
    "    pair_2 = concat([y_test_2, y_pred_2], axis=1)\n",
    "\n",
    "    n_1 = len(y_pred_1)\n",
    "    permuted_diff = []\n",
    "    for i in range(n_iterations):\n",
    "        # Pool slices and randomly split into groups of size n_1 and n_2.\n",
    "        y_H0 = shuffle(concat([pair_1, pair_2]), random_state=random_state + i)\n",
    "        y1_H0 = y_H0.iloc[:n_1]\n",
    "        y2_H0 = y_H0.iloc[n_1:]\n",
    "\n",
    "        permuted_score1 = metric(y1_H0.iloc[:, 0], y1_H0.iloc[:, 1])\n",
    "        permuted_score2 = metric(y2_H0.iloc[:, 0], y2_H0.iloc[:, 1])\n",
    "        permuted_diff.append(permuted_score1 - permuted_score2)\n",
    "\n",
    "    permuted_diff = array(permuted_diff)\n",
    "    \n",
    "    if alternative == \"greater\":\n",
    "        p_value = mean(permuted_diff >= observed_difference)\n",
    "    elif alternative == \"less\":\n",
    "        p_value = mean(permuted_diff <= observed_difference)\n",
    "    elif alternative == \"two-sided\":\n",
    "        p_value = mean(abs(permuted_diff) >= abs(observed_difference))\n",
    "\n",
    "    return observed_difference, p_value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(-0.19725842306487484, 0.058)"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "permutation_test(y_test_chemo, y_test_prob_chemo, y_test_immuno, y_test_prob_immuno, metric=roc_auc_score)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So this score is significantly worse."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ctdna + clinical characteristics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Tuning pseudo counts...\n",
      "{'pseudo_count': {<class 'ctdna.model.distributions.InflatedPinnedLogNormal'>: 1.1842203641488176, <class 'ctdna.model.distributions.Bernoulli'>: 0.5665095147278632}}\n",
      "Re-training tuned model... [DONE]\n"
     ]
    }
   ],
   "source": [
    "model = build_train_clinical_ctdna_model(X_train, y_train, X_dev, y_dev)\n",
    "features = list(model.distributions.keys())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_test_prob_immuno = model.predict_proba(X_test_immuno[features]).iloc[:,1]\n",
    "immuno_score = stratified_score(y_test_immuno, y_test_prob_immuno, metric=roc_auc_score)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>point</th>\n",
       "      <th>lower</th>\n",
       "      <th>upper</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>overall</th>\n",
       "      <td>0.800307</td>\n",
       "      <td>0.699433</td>\n",
       "      <td>0.893654</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>Anagnostou et al. ('19)</th>\n",
       "      <td>0.75</td>\n",
       "      <td>0.5</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>Nabet et al. ('20)</th>\n",
       "      <td>0.663265</td>\n",
       "      <td>0.333125</td>\n",
       "      <td>0.9375</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>Thompson et al. ('21)</th>\n",
       "      <td>0.922222</td>\n",
       "      <td>0.7</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>Weber et al. ('21)</th>\n",
       "      <td>0.72314</td>\n",
       "      <td>0.519185</td>\n",
       "      <td>0.904364</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>Zou et al. ('21)</th>\n",
       "      <td>0.916667</td>\n",
       "      <td>0.6875</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                            point     lower     upper\n",
       "overall                  0.800307  0.699433  0.893654\n",
       "Anagnostou et al. ('19)      0.75       0.5       1.0\n",
       "Nabet et al. ('20)       0.663265  0.333125    0.9375\n",
       "Thompson et al. ('21)    0.922222       0.7       1.0\n",
       "Weber et al. ('21)        0.72314  0.519185  0.904364\n",
       "Zou et al. ('21)         0.916667    0.6875       1.0"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "immuno_score"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_test_prob_chemo = model.predict_proba(X_test_chemo[features]).iloc[:,1]\n",
    "chemo_score = bootstrap_score(\n",
    "    y_true=y_test_chemo,\n",
    "    y_pred=y_test_prob_chemo,\n",
    "    metric=roc_auc_score,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'0.59^{+0.18}_{-0.17}'"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "confidence_interval_label(chemo_score).replace('$', '')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(-0.21308363243847106, 0.025)"
      ]
     },
     "execution_count": 33,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "permutation_test(y_test_chemo, y_test_prob_chemo, y_test_immuno, y_test_prob_immuno, metric=roc_auc_score)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here too, the score is significantly worse."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.9.7 64-bit",
   "language": "python",
   "name": "python397jvsc74a57bd0e7370f93d1d0cde622a1f8e1c04877d8463912d04d973331ad4851f04de6915a"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
